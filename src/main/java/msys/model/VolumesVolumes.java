package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class VolumesVolumes {
    private FluffyVolume volume;

    @JsonProperty("volume")
    public FluffyVolume getVolume() { return volume; }
    @JsonProperty("volume")
    public void setVolume(FluffyVolume value) { this.volume = value; }
}
