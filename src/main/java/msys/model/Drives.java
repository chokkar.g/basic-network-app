package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Drives {
    private List<Drive> drive;

    @JsonProperty("drive")
    public List<Drive> getDrive() { return drive; }
    @JsonProperty("drive")
    public void setDrive(List<Drive> value) { this.drive = value; }
}
