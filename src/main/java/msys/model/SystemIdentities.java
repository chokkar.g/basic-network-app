package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class SystemIdentities {
    private SystemIdentity systemIdentity;

    @JsonProperty("systemIdentity")
    public SystemIdentity getSystemIdentity() { return systemIdentity; }
    @JsonProperty("systemIdentity")
    public void setSystemIdentity(SystemIdentity value) { this.systemIdentity = value; }
}
