package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Controllers {
    private List<Controller> controller;

    @JsonProperty("controller")
    public List<Controller> getController() { return controller; }
    @JsonProperty("controller")
    public void setController(List<Controller> value) { this.controller = value; }
}
