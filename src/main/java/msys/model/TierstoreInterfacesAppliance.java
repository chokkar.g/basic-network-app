package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class TierstoreInterfacesAppliance {
    private Interfaces interfaces;
    private String applianceName;

    @JsonProperty("interfaces")
    public Interfaces getInterfaces() { return interfaces; }
    @JsonProperty("interfaces")
    public void setInterfaces(Interfaces value) { this.interfaces = value; }

    @JsonProperty("applianceName")
    public String getApplianceName() { return applianceName; }
    @JsonProperty("applianceName")
    public void setApplianceName(String value) { this.applianceName = value; }
}
