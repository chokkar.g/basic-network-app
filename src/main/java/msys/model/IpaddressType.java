package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum IpaddressType {
    AUTO_CONF, STATIC, VIRTUAL;

    @JsonValue
    public String toValue() {
        switch (this) {
        case AUTO_CONF: return "Auto Conf";
        case STATIC: return "Static";
        case VIRTUAL: return "Virtual";
        }
        return null;
    }

    @JsonCreator
    public static IpaddressType forValue(String value) throws IOException {
        if (value.equals("Auto Conf")) return AUTO_CONF;
        if (value.equals("Static")) return STATIC;
        if (value.equals("Virtual")) return VIRTUAL;
        throw new IOException("Cannot deserialize IpaddressType");
    }
}
