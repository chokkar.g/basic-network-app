package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum UUID {
    THE_600176_C18_D8_D6_E8_A3_D61_EA4_AC44637_ED, THE_600176_C1_AF4_B7_F869_DE816_EE36_CA44_A0, THE_600176_C1_B7_E80_DC46125_A9_E860_EEBD55;

    @JsonValue
    public String toValue() {
        switch (this) {
        case THE_600176_C18_D8_D6_E8_A3_D61_EA4_AC44637_ED: return "600176c18d8d6e8a3d61ea4ac44637ed";
        case THE_600176_C1_AF4_B7_F869_DE816_EE36_CA44_A0: return "600176c1af4b7f869de816ee36ca44a0";
        case THE_600176_C1_B7_E80_DC46125_A9_E860_EEBD55: return "600176c1b7e80dc46125a9e860eebd55";
        }
        return null;
    }

    @JsonCreator
    public static UUID forValue(String value) throws IOException {
        if (value.equals("600176c18d8d6e8a3d61ea4ac44637ed")) return THE_600176_C18_D8_D6_E8_A3_D61_EA4_AC44637_ED;
        if (value.equals("600176c1af4b7f869de816ee36ca44a0")) return THE_600176_C1_AF4_B7_F869_DE816_EE36_CA44_A0;
        if (value.equals("600176c1b7e80dc46125a9e860eebd55")) return THE_600176_C1_B7_E80_DC46125_A9_E860_EEBD55;
        throw new IOException("Cannot deserialize UUID");
    }
}
