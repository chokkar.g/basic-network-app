package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Pools {
    private List<Pool> pool;

    @JsonProperty("pool")
    public List<Pool> getPool() { return pool; }
    @JsonProperty("pool")
    public void setPool(List<Pool> value) { this.pool = value; }
}
