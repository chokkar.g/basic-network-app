package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Interface {
    private String taskID;
    private String networkAddress;
    private String graphIndex;
    private String interfaceName;
    private String id;

    @JsonProperty("TaskId")
    public String getTaskID() { return taskID; }
    @JsonProperty("TaskId")
    public void setTaskID(String value) { this.taskID = value; }

    @JsonProperty("NetworkAddress")
    public String getNetworkAddress() { return networkAddress; }
    @JsonProperty("NetworkAddress")
    public void setNetworkAddress(String value) { this.networkAddress = value; }

    @JsonProperty("graphIndex")
    public String getGraphIndex() { return graphIndex; }
    @JsonProperty("graphIndex")
    public void setGraphIndex(String value) { this.graphIndex = value; }

    @JsonProperty("InterfaceName")
    public String getInterfaceName() { return interfaceName; }
    @JsonProperty("InterfaceName")
    public void setInterfaceName(String value) { this.interfaceName = value; }

    @JsonProperty("id")
    public String getID() { return id; }
    @JsonProperty("id")
    public void setID(String value) { this.id = value; }
}
