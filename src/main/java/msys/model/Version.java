package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Version {
    private String hardwareName;
    private String objectPath;
    private String versionString;
    private String taskID;
    private String codeName;
    private String basetype;
    private long hotFix;
    private String hardwareRev;
    private String codeDate;
    private long codeRev;
    private String managementUtilities;
    private long major;
    private String buildDate;
    private String buildType;
    private String hotFixList;
    private String usageProfile;
    private String id;
    private long minor;
    private String versionExtra;
    private long patch;
    private String controllerUID;
    private String softwareName;
    private String softwareVendor;

    @JsonProperty("HardwareName")
    public String getHardwareName() { return hardwareName; }
    @JsonProperty("HardwareName")
    public void setHardwareName(String value) { this.hardwareName = value; }

    @JsonProperty("ObjectPath")
    public String getObjectPath() { return objectPath; }
    @JsonProperty("ObjectPath")
    public void setObjectPath(String value) { this.objectPath = value; }

    @JsonProperty("VersionString")
    public String getVersionString() { return versionString; }
    @JsonProperty("VersionString")
    public void setVersionString(String value) { this.versionString = value; }

    @JsonProperty("TaskId")
    public String getTaskID() { return taskID; }
    @JsonProperty("TaskId")
    public void setTaskID(String value) { this.taskID = value; }

    @JsonProperty("CodeName")
    public String getCodeName() { return codeName; }
    @JsonProperty("CodeName")
    public void setCodeName(String value) { this.codeName = value; }

    @JsonProperty("Basetype")
    public String getBasetype() { return basetype; }
    @JsonProperty("Basetype")
    public void setBasetype(String value) { this.basetype = value; }

    @JsonProperty("HotFix")
    public long getHotFix() { return hotFix; }
    @JsonProperty("HotFix")
    public void setHotFix(long value) { this.hotFix = value; }

    @JsonProperty("HardwareRev")
    public String getHardwareRev() { return hardwareRev; }
    @JsonProperty("HardwareRev")
    public void setHardwareRev(String value) { this.hardwareRev = value; }

    @JsonProperty("CodeDate")
    public String getCodeDate() { return codeDate; }
    @JsonProperty("CodeDate")
    public void setCodeDate(String value) { this.codeDate = value; }

    @JsonProperty("CodeRev")
    public long getCodeRev() { return codeRev; }
    @JsonProperty("CodeRev")
    public void setCodeRev(long value) { this.codeRev = value; }

    @JsonProperty("ManagementUtilities")
    public String getManagementUtilities() { return managementUtilities; }
    @JsonProperty("ManagementUtilities")
    public void setManagementUtilities(String value) { this.managementUtilities = value; }

    @JsonProperty("Major")
    public long getMajor() { return major; }
    @JsonProperty("Major")
    public void setMajor(long value) { this.major = value; }

    @JsonProperty("BuildDate")
    public String getBuildDate() { return buildDate; }
    @JsonProperty("BuildDate")
    public void setBuildDate(String value) { this.buildDate = value; }

    @JsonProperty("BuildType")
    public String getBuildType() { return buildType; }
    @JsonProperty("BuildType")
    public void setBuildType(String value) { this.buildType = value; }

    @JsonProperty("HotFixList")
    public String getHotFixList() { return hotFixList; }
    @JsonProperty("HotFixList")
    public void setHotFixList(String value) { this.hotFixList = value; }

    @JsonProperty("UsageProfile")
    public String getUsageProfile() { return usageProfile; }
    @JsonProperty("UsageProfile")
    public void setUsageProfile(String value) { this.usageProfile = value; }

    @JsonProperty("id")
    public String getID() { return id; }
    @JsonProperty("id")
    public void setID(String value) { this.id = value; }

    @JsonProperty("Minor")
    public long getMinor() { return minor; }
    @JsonProperty("Minor")
    public void setMinor(long value) { this.minor = value; }

    @JsonProperty("VersionExtra")
    public String getVersionExtra() { return versionExtra; }
    @JsonProperty("VersionExtra")
    public void setVersionExtra(String value) { this.versionExtra = value; }

    @JsonProperty("Patch")
    public long getPatch() { return patch; }
    @JsonProperty("Patch")
    public void setPatch(long value) { this.patch = value; }

    @JsonProperty("ControllerUID")
    public String getControllerUID() { return controllerUID; }
    @JsonProperty("ControllerUID")
    public void setControllerUID(String value) { this.controllerUID = value; }

    @JsonProperty("SoftwareName")
    public String getSoftwareName() { return softwareName; }
    @JsonProperty("SoftwareName")
    public void setSoftwareName(String value) { this.softwareName = value; }

    @JsonProperty("SoftwareVendor")
    public String getSoftwareVendor() { return softwareVendor; }
    @JsonProperty("SoftwareVendor")
    public void setSoftwareVendor(String value) { this.softwareVendor = value; }
}
