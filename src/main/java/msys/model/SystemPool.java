package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class SystemPool {
    private String treeViewLogicalDecorator;
    private String treeViewPhysicalDecorator;
    private String serialNumber;
    private SystemVolumes systemVolumes;
    private long type;
    private String uuid;
    private boolean isHidden;
    private UUID databankUUID;
    private long size;
    private LogicalState logicalState;
    private boolean isCompletelySpecified;
    private String treeViewEntityIcon;
    private String name;
    private long poolID;
    private long volumeCount;
    private String volumeIDs;
    private long state;

    @JsonProperty("treeViewLogicalDecorator")
    public String getTreeViewLogicalDecorator() { return treeViewLogicalDecorator; }
    @JsonProperty("treeViewLogicalDecorator")
    public void setTreeViewLogicalDecorator(String value) { this.treeViewLogicalDecorator = value; }

    @JsonProperty("treeViewPhysicalDecorator")
    public String getTreeViewPhysicalDecorator() { return treeViewPhysicalDecorator; }
    @JsonProperty("treeViewPhysicalDecorator")
    public void setTreeViewPhysicalDecorator(String value) { this.treeViewPhysicalDecorator = value; }

    @JsonProperty("serialNumber")
    public String getSerialNumber() { return serialNumber; }
    @JsonProperty("serialNumber")
    public void setSerialNumber(String value) { this.serialNumber = value; }

    @JsonProperty("systemVolumes")
    public SystemVolumes getSystemVolumes() { return systemVolumes; }
    @JsonProperty("systemVolumes")
    public void setSystemVolumes(SystemVolumes value) { this.systemVolumes = value; }

    @JsonProperty("type")
    public long getType() { return type; }
    @JsonProperty("type")
    public void setType(long value) { this.type = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("isHidden")
    public boolean getIsHidden() { return isHidden; }
    @JsonProperty("isHidden")
    public void setIsHidden(boolean value) { this.isHidden = value; }

    @JsonProperty("databankUUID")
    public UUID getDatabankUUID() { return databankUUID; }
    @JsonProperty("databankUUID")
    public void setDatabankUUID(UUID value) { this.databankUUID = value; }

    @JsonProperty("size")
    public long getSize() { return size; }
    @JsonProperty("size")
    public void setSize(long value) { this.size = value; }

    @JsonProperty("logicalState")
    public LogicalState getLogicalState() { return logicalState; }
    @JsonProperty("logicalState")
    public void setLogicalState(LogicalState value) { this.logicalState = value; }

    @JsonProperty("isCompletelySpecified")
    public boolean getIsCompletelySpecified() { return isCompletelySpecified; }
    @JsonProperty("isCompletelySpecified")
    public void setIsCompletelySpecified(boolean value) { this.isCompletelySpecified = value; }

    @JsonProperty("treeViewEntityIcon")
    public String getTreeViewEntityIcon() { return treeViewEntityIcon; }
    @JsonProperty("treeViewEntityIcon")
    public void setTreeViewEntityIcon(String value) { this.treeViewEntityIcon = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("poolID")
    public long getPoolID() { return poolID; }
    @JsonProperty("poolID")
    public void setPoolID(long value) { this.poolID = value; }

    @JsonProperty("volumeCount")
    public long getVolumeCount() { return volumeCount; }
    @JsonProperty("volumeCount")
    public void setVolumeCount(long value) { this.volumeCount = value; }

    @JsonProperty("volumeIDs")
    public String getVolumeIDs() { return volumeIDs; }
    @JsonProperty("volumeIDs")
    public void setVolumeIDs(String value) { this.volumeIDs = value; }

    @JsonProperty("state")
    public long getState() { return state; }
    @JsonProperty("state")
    public void setState(long value) { this.state = value; }
}
