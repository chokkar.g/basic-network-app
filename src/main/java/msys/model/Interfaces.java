package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Interfaces {
    private List<Interface> interfacesInterface;

    @JsonProperty("interface")
    public List<Interface> getInterfacesInterface() { return interfacesInterface; }
    @JsonProperty("interface")
    public void setInterfacesInterface(List<Interface> value) { this.interfacesInterface = value; }
}
