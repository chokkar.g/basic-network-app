package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class SystemPools {
    private List<SystemPool> systemPool;

    @JsonProperty("systemPool")
    public List<SystemPool> getSystemPool() { return systemPool; }
    @JsonProperty("systemPool")
    public void setSystemPool(List<SystemPool> value) { this.systemPool = value; }
}
