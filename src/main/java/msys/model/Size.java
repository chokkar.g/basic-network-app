package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum Size {
    THE_1_TB, THE_25_GB;

    @JsonValue
    public String toValue() {
        switch (this) {
        case THE_1_TB: return "1 TB";
        case THE_25_GB: return "> 25 GB";
        }
        return null;
    }

    @JsonCreator
    public static Size forValue(String value) throws IOException {
        if (value.equals("1 TB")) return THE_1_TB;
        if (value.equals("> 25 GB")) return THE_25_GB;
        throw new IOException("Cannot deserialize Size");
    }
}
