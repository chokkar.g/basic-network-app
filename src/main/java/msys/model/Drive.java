package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Drive {
    private Name interfacetype;
    private UUID applianceuuid;
    private long slot;
    private Technology technology;
    private String stateIcon;
    private DriveType type;
    private String uuid;
    private SpeedEnum speed;
    private Name tiertype;
    private long tier;
    private Size size;
    private MastershipState vendor;
    private Model model;
    private String location;
    private LogicalState state;
    private Firmware firmware;

    @JsonProperty("interfacetype")
    public Name getInterfacetype() { return interfacetype; }
    @JsonProperty("interfacetype")
    public void setInterfacetype(Name value) { this.interfacetype = value; }

    @JsonProperty("applianceuuid")
    public UUID getApplianceuuid() { return applianceuuid; }
    @JsonProperty("applianceuuid")
    public void setApplianceuuid(UUID value) { this.applianceuuid = value; }

    @JsonProperty("slot")
    public long getSlot() { return slot; }
    @JsonProperty("slot")
    public void setSlot(long value) { this.slot = value; }

    @JsonProperty("technology")
    public Technology getTechnology() { return technology; }
    @JsonProperty("technology")
    public void setTechnology(Technology value) { this.technology = value; }

    @JsonProperty("stateIcon")
    public String getStateIcon() { return stateIcon; }
    @JsonProperty("stateIcon")
    public void setStateIcon(String value) { this.stateIcon = value; }

    @JsonProperty("type")
    public DriveType getType() { return type; }
    @JsonProperty("type")
    public void setType(DriveType value) { this.type = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("speed")
    public SpeedEnum getSpeed() { return speed; }
    @JsonProperty("speed")
    public void setSpeed(SpeedEnum value) { this.speed = value; }

    @JsonProperty("tiertype")
    public Name getTiertype() { return tiertype; }
    @JsonProperty("tiertype")
    public void setTiertype(Name value) { this.tiertype = value; }

    @JsonProperty("tier")
    public long getTier() { return tier; }
    @JsonProperty("tier")
    public void setTier(long value) { this.tier = value; }

    @JsonProperty("size")
    public Size getSize() { return size; }
    @JsonProperty("size")
    public void setSize(Size value) { this.size = value; }

    @JsonProperty("vendor")
    public MastershipState getVendor() { return vendor; }
    @JsonProperty("vendor")
    public void setVendor(MastershipState value) { this.vendor = value; }

    @JsonProperty("model")
    public Model getModel() { return model; }
    @JsonProperty("model")
    public void setModel(Model value) { this.model = value; }

    @JsonProperty("location")
    public String getLocation() { return location; }
    @JsonProperty("location")
    public void setLocation(String value) { this.location = value; }

    @JsonProperty("state")
    public LogicalState getState() { return state; }
    @JsonProperty("state")
    public void setState(LogicalState value) { this.state = value; }

    @JsonProperty("firmware")
    public Firmware getFirmware() { return firmware; }
    @JsonProperty("firmware")
    public void setFirmware(Firmware value) { this.firmware = value; }
}
