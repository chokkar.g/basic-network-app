package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class PurplePolicy {
    private String objectPath;
    private String treeViewLogicalDecorator;
    private String taskID;
    private String displayTotalAllocated;
    private String basetype;
    private boolean readWarmEnabled;
    private PolicyVolumesClass volumes;
    private boolean writeCacheEnabled;
    private String writeCacheDescription;
    private String serviceLevelUUID;
    private String readWarmDescription;
    private String name;
    private long totalUsedMB;
    private String displayTotalUsed;
    private long bandwidthGuarantee;
    private String readAheadDescription;
    private long latencyGuarantee;
    private long level;
    private String id;
    private LogicalState state;
    private long iopsGuarantee;
    private boolean readAheadEnabled;
    private long totalAllocatedMB;

    @JsonProperty("ObjectPath")
    public String getObjectPath() { return objectPath; }
    @JsonProperty("ObjectPath")
    public void setObjectPath(String value) { this.objectPath = value; }

    @JsonProperty("treeViewLogicalDecorator")
    public String getTreeViewLogicalDecorator() { return treeViewLogicalDecorator; }
    @JsonProperty("treeViewLogicalDecorator")
    public void setTreeViewLogicalDecorator(String value) { this.treeViewLogicalDecorator = value; }

    @JsonProperty("TaskId")
    public String getTaskID() { return taskID; }
    @JsonProperty("TaskId")
    public void setTaskID(String value) { this.taskID = value; }

    @JsonProperty("DisplayTotalAllocated")
    public String getDisplayTotalAllocated() { return displayTotalAllocated; }
    @JsonProperty("DisplayTotalAllocated")
    public void setDisplayTotalAllocated(String value) { this.displayTotalAllocated = value; }

    @JsonProperty("Basetype")
    public String getBasetype() { return basetype; }
    @JsonProperty("Basetype")
    public void setBasetype(String value) { this.basetype = value; }

    @JsonProperty("ReadWarmEnabled")
    public boolean getReadWarmEnabled() { return readWarmEnabled; }
    @JsonProperty("ReadWarmEnabled")
    public void setReadWarmEnabled(boolean value) { this.readWarmEnabled = value; }

    @JsonProperty("volumes")
    public PolicyVolumesClass getVolumes() { return volumes; }
    @JsonProperty("volumes")
    public void setVolumes(PolicyVolumesClass value) { this.volumes = value; }

    @JsonProperty("WriteCacheEnabled")
    public boolean getWriteCacheEnabled() { return writeCacheEnabled; }
    @JsonProperty("WriteCacheEnabled")
    public void setWriteCacheEnabled(boolean value) { this.writeCacheEnabled = value; }

    @JsonProperty("WriteCacheDescription")
    public String getWriteCacheDescription() { return writeCacheDescription; }
    @JsonProperty("WriteCacheDescription")
    public void setWriteCacheDescription(String value) { this.writeCacheDescription = value; }

    @JsonProperty("ServiceLevelUUID")
    public String getServiceLevelUUID() { return serviceLevelUUID; }
    @JsonProperty("ServiceLevelUUID")
    public void setServiceLevelUUID(String value) { this.serviceLevelUUID = value; }

    @JsonProperty("ReadWarmDescription")
    public String getReadWarmDescription() { return readWarmDescription; }
    @JsonProperty("ReadWarmDescription")
    public void setReadWarmDescription(String value) { this.readWarmDescription = value; }

    @JsonProperty("Name")
    public String getName() { return name; }
    @JsonProperty("Name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("TotalUsedMB")
    public long getTotalUsedMB() { return totalUsedMB; }
    @JsonProperty("TotalUsedMB")
    public void setTotalUsedMB(long value) { this.totalUsedMB = value; }

    @JsonProperty("DisplayTotalUsed")
    public String getDisplayTotalUsed() { return displayTotalUsed; }
    @JsonProperty("DisplayTotalUsed")
    public void setDisplayTotalUsed(String value) { this.displayTotalUsed = value; }

    @JsonProperty("BandwidthGuarantee")
    public long getBandwidthGuarantee() { return bandwidthGuarantee; }
    @JsonProperty("BandwidthGuarantee")
    public void setBandwidthGuarantee(long value) { this.bandwidthGuarantee = value; }

    @JsonProperty("ReadAheadDescription")
    public String getReadAheadDescription() { return readAheadDescription; }
    @JsonProperty("ReadAheadDescription")
    public void setReadAheadDescription(String value) { this.readAheadDescription = value; }

    @JsonProperty("LatencyGuarantee")
    public long getLatencyGuarantee() { return latencyGuarantee; }
    @JsonProperty("LatencyGuarantee")
    public void setLatencyGuarantee(long value) { this.latencyGuarantee = value; }

    @JsonProperty("Level")
    public long getLevel() { return level; }
    @JsonProperty("Level")
    public void setLevel(long value) { this.level = value; }

    @JsonProperty("id")
    public String getID() { return id; }
    @JsonProperty("id")
    public void setID(String value) { this.id = value; }

    @JsonProperty("state")
    public LogicalState getState() { return state; }
    @JsonProperty("state")
    public void setState(LogicalState value) { this.state = value; }

    @JsonProperty("IopsGuarantee")
    public long getIopsGuarantee() { return iopsGuarantee; }
    @JsonProperty("IopsGuarantee")
    public void setIopsGuarantee(long value) { this.iopsGuarantee = value; }

    @JsonProperty("ReadAheadEnabled")
    public boolean getReadAheadEnabled() { return readAheadEnabled; }
    @JsonProperty("ReadAheadEnabled")
    public void setReadAheadEnabled(boolean value) { this.readAheadEnabled = value; }

    @JsonProperty("TotalAllocatedMB")
    public long getTotalAllocatedMB() { return totalAllocatedMB; }
    @JsonProperty("TotalAllocatedMB")
    public void setTotalAllocatedMB(long value) { this.totalAllocatedMB = value; }
}
