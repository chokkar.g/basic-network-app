package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum Subnetmask {
    FFFF_FFFF_FFFF_FFFF_0000, THE_2552551920, THE_2552552550;

    @JsonValue
    public String toValue() {
        switch (this) {
        case FFFF_FFFF_FFFF_FFFF_0000: return "FFFF:FFFF:FFFF:FFFF:0:0:0:0";
        case THE_2552551920: return "255.255.192.0";
        case THE_2552552550: return "255.255.255.0";
        }
        return null;
    }

    @JsonCreator
    public static Subnetmask forValue(String value) throws IOException {
        if (value.equals("FFFF:FFFF:FFFF:FFFF:0:0:0:0")) return FFFF_FFFF_FFFF_FFFF_0000;
        if (value.equals("255.255.192.0")) return THE_2552551920;
        if (value.equals("255.255.255.0")) return THE_2552552550;
        throw new IOException("Cannot deserialize Subnetmask");
    }
}
