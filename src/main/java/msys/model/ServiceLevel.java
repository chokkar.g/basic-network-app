package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class ServiceLevel {
    private long totalUsedMB;
    private String objectPath;
    private String treeViewLogicalDecorator;
    private String taskID;
    private String displayTotalAllocated;
    private String displayTotalUsed;
    private String basetype;
    private Policies policies;
    private String id;
    private LogicalState state;
    private long totalAllocatedMB;
    private String name;

    @JsonProperty("TotalUsedMB")
    public long getTotalUsedMB() { return totalUsedMB; }
    @JsonProperty("TotalUsedMB")
    public void setTotalUsedMB(long value) { this.totalUsedMB = value; }

    @JsonProperty("ObjectPath")
    public String getObjectPath() { return objectPath; }
    @JsonProperty("ObjectPath")
    public void setObjectPath(String value) { this.objectPath = value; }

    @JsonProperty("treeViewLogicalDecorator")
    public String getTreeViewLogicalDecorator() { return treeViewLogicalDecorator; }
    @JsonProperty("treeViewLogicalDecorator")
    public void setTreeViewLogicalDecorator(String value) { this.treeViewLogicalDecorator = value; }

    @JsonProperty("TaskId")
    public String getTaskID() { return taskID; }
    @JsonProperty("TaskId")
    public void setTaskID(String value) { this.taskID = value; }

    @JsonProperty("DisplayTotalAllocated")
    public String getDisplayTotalAllocated() { return displayTotalAllocated; }
    @JsonProperty("DisplayTotalAllocated")
    public void setDisplayTotalAllocated(String value) { this.displayTotalAllocated = value; }

    @JsonProperty("DisplayTotalUsed")
    public String getDisplayTotalUsed() { return displayTotalUsed; }
    @JsonProperty("DisplayTotalUsed")
    public void setDisplayTotalUsed(String value) { this.displayTotalUsed = value; }

    @JsonProperty("Basetype")
    public String getBasetype() { return basetype; }
    @JsonProperty("Basetype")
    public void setBasetype(String value) { this.basetype = value; }

    @JsonProperty("policies")
    public Policies getPolicies() { return policies; }
    @JsonProperty("policies")
    public void setPolicies(Policies value) { this.policies = value; }

    @JsonProperty("id")
    public String getID() { return id; }
    @JsonProperty("id")
    public void setID(String value) { this.id = value; }

    @JsonProperty("state")
    public LogicalState getState() { return state; }
    @JsonProperty("state")
    public void setState(LogicalState value) { this.state = value; }

    @JsonProperty("TotalAllocatedMB")
    public long getTotalAllocatedMB() { return totalAllocatedMB; }
    @JsonProperty("TotalAllocatedMB")
    public void setTotalAllocatedMB(long value) { this.totalAllocatedMB = value; }

    @JsonProperty("Name")
    public String getName() { return name; }
    @JsonProperty("Name")
    public void setName(String value) { this.name = value; }
}
