package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Nics {
    private List<NIC> nic;

    @JsonProperty("nic")
    public List<NIC> getNIC() { return nic; }
    @JsonProperty("nic")
    public void setNIC(List<NIC> value) { this.nic = value; }
}
