package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class UserVolumes {
    private List<ServiceLevel> serviceLevel;

    @JsonProperty("serviceLevel")
    public List<ServiceLevel> getServiceLevel() { return serviceLevel; }
    @JsonProperty("serviceLevel")
    public void setServiceLevel(List<ServiceLevel> value) { this.serviceLevel = value; }
}
