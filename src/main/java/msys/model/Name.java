package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum Name {
    CACHE, SATA;

    @JsonValue
    public String toValue() {
        switch (this) {
        case CACHE: return "Cache";
        case SATA: return "SATA";
        }
        return null;
    }

    @JsonCreator
    public static Name forValue(String value) throws IOException {
        if (value.equals("Cache")) return CACHE;
        if (value.equals("SATA")) return SATA;
        throw new IOException("Cannot deserialize Name");
    }
}
