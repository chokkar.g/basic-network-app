package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Arrays {
    private Array array;

    @JsonProperty("array")
    public Array getArray() { return array; }
    @JsonProperty("array")
    public void setArray(Array value) { this.array = value; }
	@Override
	public String toString() {
		return "Arrays [array=" + array + "]";
	}
    
    
}
