package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Capacity {
    private long usable;
    private long hotspare;
    private long protection;
    private double utilization;
    private long allocated;

    @JsonProperty("usable")
    public long getUsable() { return usable; }
    @JsonProperty("usable")
    public void setUsable(long value) { this.usable = value; }

    @JsonProperty("hotspare")
    public long getHotspare() { return hotspare; }
    @JsonProperty("hotspare")
    public void setHotspare(long value) { this.hotspare = value; }

    @JsonProperty("protection")
    public long getProtection() { return protection; }
    @JsonProperty("protection")
    public void setProtection(long value) { this.protection = value; }

    @JsonProperty("utilization")
    public double getUtilization() { return utilization; }
    @JsonProperty("utilization")
    public void setUtilization(double value) { this.utilization = value; }

    @JsonProperty("allocated")
    public long getAllocated() { return allocated; }
    @JsonProperty("allocated")
    public void setAllocated(long value) { this.allocated = value; }
}
