package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class NvmeInfo {
    private String serialNumber;
    private String size;
    private long wear;
    private String model;
    private String location;
    private LogicalState state;
    private String firmware;

    @JsonProperty("serialNumber")
    public String getSerialNumber() { return serialNumber; }
    @JsonProperty("serialNumber")
    public void setSerialNumber(String value) { this.serialNumber = value; }

    @JsonProperty("size")
    public String getSize() { return size; }
    @JsonProperty("size")
    public void setSize(String value) { this.size = value; }

    @JsonProperty("wear")
    public long getWear() { return wear; }
    @JsonProperty("wear")
    public void setWear(long value) { this.wear = value; }

    @JsonProperty("model")
    public String getModel() { return model; }
    @JsonProperty("model")
    public void setModel(String value) { this.model = value; }

    @JsonProperty("location")
    public String getLocation() { return location; }
    @JsonProperty("location")
    public void setLocation(String value) { this.location = value; }

    @JsonProperty("state")
    public LogicalState getState() { return state; }
    @JsonProperty("state")
    public void setState(LogicalState value) { this.state = value; }

    @JsonProperty("firmware")
    public String getFirmware() { return firmware; }
    @JsonProperty("firmware")
    public void setFirmware(String value) { this.firmware = value; }
}
