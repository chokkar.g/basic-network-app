package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum LogicalState {
    NORMAL, N_A;

    @JsonValue
    public String toValue() {
        switch (this) {
        case NORMAL: return "Normal";
        case N_A: return "N/A";
        }
        return null;
    }

    @JsonCreator
    public static LogicalState forValue(String value) throws IOException {
        if (value.equals("Normal")) return NORMAL;
        if (value.equals("N/A")) return N_A;
        throw new IOException("Cannot deserialize LogicalState");
    }
}
