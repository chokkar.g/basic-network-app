package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class SystemIdentity {
    private String taskID;
    private String basetype;
    private String systemIdentity;

    @JsonProperty("TaskId")
    public String getTaskID() { return taskID; }
    @JsonProperty("TaskId")
    public void setTaskID(String value) { this.taskID = value; }

    @JsonProperty("Basetype")
    public String getBasetype() { return basetype; }
    @JsonProperty("Basetype")
    public void setBasetype(String value) { this.basetype = value; }

    @JsonProperty("SystemIdentity")
    public String getSystemIdentity() { return systemIdentity; }
    @JsonProperty("SystemIdentity")
    public void setSystemIdentity(String value) { this.systemIdentity = value; }
}
