package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Pool {
    private String objectPath;
    private long pagingUsedMB;
    private String taskID;
    private String basetype;
    private long size;
    private long pagingTotalMB;
    private String currentOwnerUUID;
    private long usedMetaMB;
    private long volumeCount;
    private String name;
    private long snapshotCount;
    private boolean ready;
    private long maxVolumeSizeMB;
    private long layerCount;
    private long sizeMB;
    private long usedSnapMB;
    private long usedVolumeMB;
    private String id;
    private long totalMB;
    private long usedMB;
    private long exportedVolumeMB;

    @JsonProperty("ObjectPath")
    public String getObjectPath() { return objectPath; }
    @JsonProperty("ObjectPath")
    public void setObjectPath(String value) { this.objectPath = value; }

    @JsonProperty("PagingUsedMB")
    public long getPagingUsedMB() { return pagingUsedMB; }
    @JsonProperty("PagingUsedMB")
    public void setPagingUsedMB(long value) { this.pagingUsedMB = value; }

    @JsonProperty("TaskId")
    public String getTaskID() { return taskID; }
    @JsonProperty("TaskId")
    public void setTaskID(String value) { this.taskID = value; }

    @JsonProperty("Basetype")
    public String getBasetype() { return basetype; }
    @JsonProperty("Basetype")
    public void setBasetype(String value) { this.basetype = value; }

    @JsonProperty("Size")
    public long getSize() { return size; }
    @JsonProperty("Size")
    public void setSize(long value) { this.size = value; }

    @JsonProperty("PagingTotalMB")
    public long getPagingTotalMB() { return pagingTotalMB; }
    @JsonProperty("PagingTotalMB")
    public void setPagingTotalMB(long value) { this.pagingTotalMB = value; }

    @JsonProperty("CurrentOwnerUUID")
    public String getCurrentOwnerUUID() { return currentOwnerUUID; }
    @JsonProperty("CurrentOwnerUUID")
    public void setCurrentOwnerUUID(String value) { this.currentOwnerUUID = value; }

    @JsonProperty("UsedMetaMB")
    public long getUsedMetaMB() { return usedMetaMB; }
    @JsonProperty("UsedMetaMB")
    public void setUsedMetaMB(long value) { this.usedMetaMB = value; }

    @JsonProperty("VolumeCount")
    public long getVolumeCount() { return volumeCount; }
    @JsonProperty("VolumeCount")
    public void setVolumeCount(long value) { this.volumeCount = value; }

    @JsonProperty("Name")
    public String getName() { return name; }
    @JsonProperty("Name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("SnapshotCount")
    public long getSnapshotCount() { return snapshotCount; }
    @JsonProperty("SnapshotCount")
    public void setSnapshotCount(long value) { this.snapshotCount = value; }

    @JsonProperty("Ready")
    public boolean getReady() { return ready; }
    @JsonProperty("Ready")
    public void setReady(boolean value) { this.ready = value; }

    @JsonProperty("MaxVolumeSizeMB")
    public long getMaxVolumeSizeMB() { return maxVolumeSizeMB; }
    @JsonProperty("MaxVolumeSizeMB")
    public void setMaxVolumeSizeMB(long value) { this.maxVolumeSizeMB = value; }

    @JsonProperty("LayerCount")
    public long getLayerCount() { return layerCount; }
    @JsonProperty("LayerCount")
    public void setLayerCount(long value) { this.layerCount = value; }

    @JsonProperty("SizeMB")
    public long getSizeMB() { return sizeMB; }
    @JsonProperty("SizeMB")
    public void setSizeMB(long value) { this.sizeMB = value; }

    @JsonProperty("UsedSnapMB")
    public long getUsedSnapMB() { return usedSnapMB; }
    @JsonProperty("UsedSnapMB")
    public void setUsedSnapMB(long value) { this.usedSnapMB = value; }

    @JsonProperty("UsedVolumeMB")
    public long getUsedVolumeMB() { return usedVolumeMB; }
    @JsonProperty("UsedVolumeMB")
    public void setUsedVolumeMB(long value) { this.usedVolumeMB = value; }

    @JsonProperty("id")
    public String getID() { return id; }
    @JsonProperty("id")
    public void setID(String value) { this.id = value; }

    @JsonProperty("TotalMB")
    public long getTotalMB() { return totalMB; }
    @JsonProperty("TotalMB")
    public void setTotalMB(long value) { this.totalMB = value; }

    @JsonProperty("UsedMB")
    public long getUsedMB() { return usedMB; }
    @JsonProperty("UsedMB")
    public void setUsedMB(long value) { this.usedMB = value; }

    @JsonProperty("ExportedVolumeMB")
    public long getExportedVolumeMB() { return exportedVolumeMB; }
    @JsonProperty("ExportedVolumeMB")
    public void setExportedVolumeMB(long value) { this.exportedVolumeMB = value; }
}
