package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum Gateway {
    EMPTY, THE_0000, THE_1010501, THE_1010601, THE_1011101;

    @JsonValue
    public String toValue() {
        switch (this) {
        case EMPTY: return "::";
        case THE_0000: return "0.0.0.0";
        case THE_1010501: return "10.105.0.1";
        case THE_1010601: return "10.106.0.1";
        case THE_1011101: return "10.111.0.1";
        }
        return null;
    }

    @JsonCreator
    public static Gateway forValue(String value) throws IOException {
        if (value.equals("::")) return EMPTY;
        if (value.equals("0.0.0.0")) return THE_0000;
        if (value.equals("10.105.0.1")) return THE_1010501;
        if (value.equals("10.106.0.1")) return THE_1010601;
        if (value.equals("10.111.0.1")) return THE_1011101;
        throw new IOException("Cannot deserialize Gateway");
    }
}
