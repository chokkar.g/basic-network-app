package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.*;

@JsonDeserialize(using = SpeedUnion.Deserializer.class)
@JsonSerialize(using = SpeedUnion.Serializer.class)
public class SpeedUnion {
    public Long integerValue;
    public LogicalState enumValue;

    static class Deserializer extends JsonDeserializer<SpeedUnion> {
        @Override
        public SpeedUnion deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            SpeedUnion value = new SpeedUnion();
            switch (jsonParser.getCurrentToken()) {
            case VALUE_NUMBER_INT:
                value.integerValue = jsonParser.readValueAs(Long.class);
                break;
            case VALUE_STRING:
                value.enumValue = jsonParser.readValueAs(LogicalState.class);
                break;
            default: throw new IOException("Cannot deserialize SpeedUnion");
            }
            return value;
        }
    }

    static class Serializer extends JsonSerializer<SpeedUnion> {
        @Override
        public void serialize(SpeedUnion obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            if (obj.integerValue != null) {
                jsonGenerator.writeObject(obj.integerValue);
                return;
            }
            if (obj.enumValue != null) {
                jsonGenerator.writeObject(obj.enumValue);
                return;
            }
            throw new IOException("SpeedUnion must not be null");
        }
    }
}
