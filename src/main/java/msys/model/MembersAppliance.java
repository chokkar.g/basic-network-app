package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class MembersAppliance {
    private long vmaStatus;
    private long upgradeProtocol;
    private long type;
    private boolean isAccelerationNode;
    private UUID uuid;
    private long capacity;
    private LogicalState logicalState;
    private boolean isCompletelySpecified;
    private String treeViewEntityIcon;
    private String vendor;
    private String ngsState;
    private String displayLatestVersion;
    private String model;
    private String usbSerialNumber;
    private long zduProtocol;
    private LogicalState state;
    private Physical physical;
    private String upgradestate;
    private String vmaStatusDisplay;
    private String treeViewLogicalDecorator;
    private String treeViewPhysicalDecorator;
    private String displaymodel;
    private long ngsOpMode;
    private boolean isHidden;
    private boolean evalLicense;
    private String license;
    private String arrayuuid;
    private String serial;
    private String latestVersion;
    private String name;
    private boolean assigned;
    private String upgradestateicon;
    private boolean complete;
    private boolean activated;

    @JsonProperty("vmaStatus")
    public long getVmaStatus() { return vmaStatus; }
    @JsonProperty("vmaStatus")
    public void setVmaStatus(long value) { this.vmaStatus = value; }

    @JsonProperty("upgradeProtocol")
    public long getUpgradeProtocol() { return upgradeProtocol; }
    @JsonProperty("upgradeProtocol")
    public void setUpgradeProtocol(long value) { this.upgradeProtocol = value; }

    @JsonProperty("type")
    public long getType() { return type; }
    @JsonProperty("type")
    public void setType(long value) { this.type = value; }

    @JsonProperty("isAccelerationNode")
    public boolean getIsAccelerationNode() { return isAccelerationNode; }
    @JsonProperty("isAccelerationNode")
    public void setIsAccelerationNode(boolean value) { this.isAccelerationNode = value; }

    @JsonProperty("uuid")
    public UUID getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(UUID value) { this.uuid = value; }

    @JsonProperty("capacity")
    public long getCapacity() { return capacity; }
    @JsonProperty("capacity")
    public void setCapacity(long value) { this.capacity = value; }

    @JsonProperty("logicalState")
    public LogicalState getLogicalState() { return logicalState; }
    @JsonProperty("logicalState")
    public void setLogicalState(LogicalState value) { this.logicalState = value; }

    @JsonProperty("isCompletelySpecified")
    public boolean getIsCompletelySpecified() { return isCompletelySpecified; }
    @JsonProperty("isCompletelySpecified")
    public void setIsCompletelySpecified(boolean value) { this.isCompletelySpecified = value; }

    @JsonProperty("treeViewEntityIcon")
    public String getTreeViewEntityIcon() { return treeViewEntityIcon; }
    @JsonProperty("treeViewEntityIcon")
    public void setTreeViewEntityIcon(String value) { this.treeViewEntityIcon = value; }

    @JsonProperty("vendor")
    public String getVendor() { return vendor; }
    @JsonProperty("vendor")
    public void setVendor(String value) { this.vendor = value; }

    @JsonProperty("NGSState")
    public String getNgsState() { return ngsState; }
    @JsonProperty("NGSState")
    public void setNgsState(String value) { this.ngsState = value; }

    @JsonProperty("displayLatestVersion")
    public String getDisplayLatestVersion() { return displayLatestVersion; }
    @JsonProperty("displayLatestVersion")
    public void setDisplayLatestVersion(String value) { this.displayLatestVersion = value; }

    @JsonProperty("model")
    public String getModel() { return model; }
    @JsonProperty("model")
    public void setModel(String value) { this.model = value; }

    @JsonProperty("usbSerialNumber")
    public String getUSBSerialNumber() { return usbSerialNumber; }
    @JsonProperty("usbSerialNumber")
    public void setUSBSerialNumber(String value) { this.usbSerialNumber = value; }

    @JsonProperty("zduProtocol")
    public long getZduProtocol() { return zduProtocol; }
    @JsonProperty("zduProtocol")
    public void setZduProtocol(long value) { this.zduProtocol = value; }

    @JsonProperty("state")
    public LogicalState getState() { return state; }
    @JsonProperty("state")
    public void setState(LogicalState value) { this.state = value; }

    @JsonProperty("physical")
    public Physical getPhysical() { return physical; }
    @JsonProperty("physical")
    public void setPhysical(Physical value) { this.physical = value; }

    @JsonProperty("upgradestate")
    public String getUpgradestate() { return upgradestate; }
    @JsonProperty("upgradestate")
    public void setUpgradestate(String value) { this.upgradestate = value; }

    @JsonProperty("vmaStatusDisplay")
    public String getVmaStatusDisplay() { return vmaStatusDisplay; }
    @JsonProperty("vmaStatusDisplay")
    public void setVmaStatusDisplay(String value) { this.vmaStatusDisplay = value; }

    @JsonProperty("treeViewLogicalDecorator")
    public String getTreeViewLogicalDecorator() { return treeViewLogicalDecorator; }
    @JsonProperty("treeViewLogicalDecorator")
    public void setTreeViewLogicalDecorator(String value) { this.treeViewLogicalDecorator = value; }

    @JsonProperty("treeViewPhysicalDecorator")
    public String getTreeViewPhysicalDecorator() { return treeViewPhysicalDecorator; }
    @JsonProperty("treeViewPhysicalDecorator")
    public void setTreeViewPhysicalDecorator(String value) { this.treeViewPhysicalDecorator = value; }

    @JsonProperty("displaymodel")
    public String getDisplaymodel() { return displaymodel; }
    @JsonProperty("displaymodel")
    public void setDisplaymodel(String value) { this.displaymodel = value; }

    @JsonProperty("NGSOpMode")
    public long getNgsOpMode() { return ngsOpMode; }
    @JsonProperty("NGSOpMode")
    public void setNgsOpMode(long value) { this.ngsOpMode = value; }

    @JsonProperty("isHidden")
    public boolean getIsHidden() { return isHidden; }
    @JsonProperty("isHidden")
    public void setIsHidden(boolean value) { this.isHidden = value; }

    @JsonProperty("evalLicense")
    public boolean getEvalLicense() { return evalLicense; }
    @JsonProperty("evalLicense")
    public void setEvalLicense(boolean value) { this.evalLicense = value; }

    @JsonProperty("license")
    public String getLicense() { return license; }
    @JsonProperty("license")
    public void setLicense(String value) { this.license = value; }

    @JsonProperty("arrayuuid")
    public String getArrayuuid() { return arrayuuid; }
    @JsonProperty("arrayuuid")
    public void setArrayuuid(String value) { this.arrayuuid = value; }

    @JsonProperty("serial")
    public String getSerial() { return serial; }
    @JsonProperty("serial")
    public void setSerial(String value) { this.serial = value; }

    @JsonProperty("latestVersion")
    public String getLatestVersion() { return latestVersion; }
    @JsonProperty("latestVersion")
    public void setLatestVersion(String value) { this.latestVersion = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("assigned")
    public boolean getAssigned() { return assigned; }
    @JsonProperty("assigned")
    public void setAssigned(boolean value) { this.assigned = value; }

    @JsonProperty("upgradestateicon")
    public String getUpgradestateicon() { return upgradestateicon; }
    @JsonProperty("upgradestateicon")
    public void setUpgradestateicon(String value) { this.upgradestateicon = value; }

    @JsonProperty("complete")
    public boolean getComplete() { return complete; }
    @JsonProperty("complete")
    public void setComplete(boolean value) { this.complete = value; }

    @JsonProperty("activated")
    public boolean getActivated() { return activated; }
    @JsonProperty("activated")
    public void setActivated(boolean value) { this.activated = value; }
}
