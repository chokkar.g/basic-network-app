package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class TentacledVolume {
    private long size;
    private String aclGroup;
    private long snapshotRetention;
    private long dupedSize;
    private long aclGroupID;
    private String name;
    private String provisioningType;
    private String layerID;
    private long metadataSizeMB;
    private ParentLayerID parentLayerID;
    private long highWaterMarkMB;
    private String previousEncryptionKey;
    private String id;
    private LogicalState state;
    private String currentEncryptionKey;
    private long dupedSizeMB;
    private String protectionPolicy;
    private long keyServerClusterID;
    private String currentOwnerUUID;
    private String iqn;
    private String keyServerClusterName;
    private long logicalUnitNumber;
    private String encryptedStatusLong;
    private double allowedTierMask;
    private String affinity;
    private String percentFree;
    private long numberOfPagesPerChapter;
    private String policyUUID;
    private String objectPath;
    private String poolUUID;
    private String scheduleGroupUUID;
    private String taskID;
    private long physicalSizeMB;
    private long createDateTime;
    private String replicaPeerInfo;
    private long sizeMB;
    private String displaySize;
    private String treeViewEntityIcon;
    private String displayName;
    private long highWaterMark;
    private String protocol;
    private long requiredTierMask;
    private String scsiVpdNaa;
    private String encryptedStatusShort;
    private String treeViewLogicalDecorator;
    private String basetype;
    private long faultTolerance;
    private String displayGroupID;
    private ConnectedInitiatorList connectedInitiatorList;
    private String displayDateTime;
    private long metadataSize;
    private long snapshotCount;
    private long pathTolerance;
    private long physicalSize;

    @JsonProperty("Size")
    public long getSize() { return size; }
    @JsonProperty("Size")
    public void setSize(long value) { this.size = value; }

    @JsonProperty("ACLGroup")
    public String getACLGroup() { return aclGroup; }
    @JsonProperty("ACLGroup")
    public void setACLGroup(String value) { this.aclGroup = value; }

    @JsonProperty("SnapshotRetention")
    public long getSnapshotRetention() { return snapshotRetention; }
    @JsonProperty("SnapshotRetention")
    public void setSnapshotRetention(long value) { this.snapshotRetention = value; }

    @JsonProperty("DupedSize")
    public long getDupedSize() { return dupedSize; }
    @JsonProperty("DupedSize")
    public void setDupedSize(long value) { this.dupedSize = value; }

    @JsonProperty("ACLGroupID")
    public long getACLGroupID() { return aclGroupID; }
    @JsonProperty("ACLGroupID")
    public void setACLGroupID(long value) { this.aclGroupID = value; }

    @JsonProperty("Name")
    public String getName() { return name; }
    @JsonProperty("Name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("ProvisioningType")
    public String getProvisioningType() { return provisioningType; }
    @JsonProperty("ProvisioningType")
    public void setProvisioningType(String value) { this.provisioningType = value; }

    @JsonProperty("LayerId")
    public String getLayerID() { return layerID; }
    @JsonProperty("LayerId")
    public void setLayerID(String value) { this.layerID = value; }

    @JsonProperty("MetadataSizeMB")
    public long getMetadataSizeMB() { return metadataSizeMB; }
    @JsonProperty("MetadataSizeMB")
    public void setMetadataSizeMB(long value) { this.metadataSizeMB = value; }

    @JsonProperty("ParentLayerId")
    public ParentLayerID getParentLayerID() { return parentLayerID; }
    @JsonProperty("ParentLayerId")
    public void setParentLayerID(ParentLayerID value) { this.parentLayerID = value; }

    @JsonProperty("HighWaterMarkMB")
    public long getHighWaterMarkMB() { return highWaterMarkMB; }
    @JsonProperty("HighWaterMarkMB")
    public void setHighWaterMarkMB(long value) { this.highWaterMarkMB = value; }

    @JsonProperty("PreviousEncryptionKey")
    public String getPreviousEncryptionKey() { return previousEncryptionKey; }
    @JsonProperty("PreviousEncryptionKey")
    public void setPreviousEncryptionKey(String value) { this.previousEncryptionKey = value; }

    @JsonProperty("id")
    public String getID() { return id; }
    @JsonProperty("id")
    public void setID(String value) { this.id = value; }

    @JsonProperty("state")
    public LogicalState getState() { return state; }
    @JsonProperty("state")
    public void setState(LogicalState value) { this.state = value; }

    @JsonProperty("CurrentEncryptionKey")
    public String getCurrentEncryptionKey() { return currentEncryptionKey; }
    @JsonProperty("CurrentEncryptionKey")
    public void setCurrentEncryptionKey(String value) { this.currentEncryptionKey = value; }

    @JsonProperty("DupedSizeMB")
    public long getDupedSizeMB() { return dupedSizeMB; }
    @JsonProperty("DupedSizeMB")
    public void setDupedSizeMB(long value) { this.dupedSizeMB = value; }

    @JsonProperty("ProtectionPolicy")
    public String getProtectionPolicy() { return protectionPolicy; }
    @JsonProperty("ProtectionPolicy")
    public void setProtectionPolicy(String value) { this.protectionPolicy = value; }

    @JsonProperty("KeyServerClusterID")
    public long getKeyServerClusterID() { return keyServerClusterID; }
    @JsonProperty("KeyServerClusterID")
    public void setKeyServerClusterID(long value) { this.keyServerClusterID = value; }

    @JsonProperty("CurrentOwnerUUID")
    public String getCurrentOwnerUUID() { return currentOwnerUUID; }
    @JsonProperty("CurrentOwnerUUID")
    public void setCurrentOwnerUUID(String value) { this.currentOwnerUUID = value; }

    @JsonProperty("IQN")
    public String getIqn() { return iqn; }
    @JsonProperty("IQN")
    public void setIqn(String value) { this.iqn = value; }

    @JsonProperty("KeyServerClusterName")
    public String getKeyServerClusterName() { return keyServerClusterName; }
    @JsonProperty("KeyServerClusterName")
    public void setKeyServerClusterName(String value) { this.keyServerClusterName = value; }

    @JsonProperty("LogicalUnitNumber")
    public long getLogicalUnitNumber() { return logicalUnitNumber; }
    @JsonProperty("LogicalUnitNumber")
    public void setLogicalUnitNumber(long value) { this.logicalUnitNumber = value; }

    @JsonProperty("EncryptedStatusLong")
    public String getEncryptedStatusLong() { return encryptedStatusLong; }
    @JsonProperty("EncryptedStatusLong")
    public void setEncryptedStatusLong(String value) { this.encryptedStatusLong = value; }

    @JsonProperty("AllowedTierMask")
    public double getAllowedTierMask() { return allowedTierMask; }
    @JsonProperty("AllowedTierMask")
    public void setAllowedTierMask(double value) { this.allowedTierMask = value; }

    @JsonProperty("Affinity")
    public String getAffinity() { return affinity; }
    @JsonProperty("Affinity")
    public void setAffinity(String value) { this.affinity = value; }

    @JsonProperty("PercentFree")
    public String getPercentFree() { return percentFree; }
    @JsonProperty("PercentFree")
    public void setPercentFree(String value) { this.percentFree = value; }

    @JsonProperty("NumberOfPagesPerChapter")
    public long getNumberOfPagesPerChapter() { return numberOfPagesPerChapter; }
    @JsonProperty("NumberOfPagesPerChapter")
    public void setNumberOfPagesPerChapter(long value) { this.numberOfPagesPerChapter = value; }

    @JsonProperty("PolicyUUID")
    public String getPolicyUUID() { return policyUUID; }
    @JsonProperty("PolicyUUID")
    public void setPolicyUUID(String value) { this.policyUUID = value; }

    @JsonProperty("ObjectPath")
    public String getObjectPath() { return objectPath; }
    @JsonProperty("ObjectPath")
    public void setObjectPath(String value) { this.objectPath = value; }

    @JsonProperty("PoolUUID")
    public String getPoolUUID() { return poolUUID; }
    @JsonProperty("PoolUUID")
    public void setPoolUUID(String value) { this.poolUUID = value; }

    @JsonProperty("ScheduleGroupUUID")
    public String getScheduleGroupUUID() { return scheduleGroupUUID; }
    @JsonProperty("ScheduleGroupUUID")
    public void setScheduleGroupUUID(String value) { this.scheduleGroupUUID = value; }

    @JsonProperty("TaskId")
    public String getTaskID() { return taskID; }
    @JsonProperty("TaskId")
    public void setTaskID(String value) { this.taskID = value; }

    @JsonProperty("PhysicalSizeMB")
    public long getPhysicalSizeMB() { return physicalSizeMB; }
    @JsonProperty("PhysicalSizeMB")
    public void setPhysicalSizeMB(long value) { this.physicalSizeMB = value; }

    @JsonProperty("CreateDateTime")
    public long getCreateDateTime() { return createDateTime; }
    @JsonProperty("CreateDateTime")
    public void setCreateDateTime(long value) { this.createDateTime = value; }

    @JsonProperty("ReplicaPeerInfo")
    public String getReplicaPeerInfo() { return replicaPeerInfo; }
    @JsonProperty("ReplicaPeerInfo")
    public void setReplicaPeerInfo(String value) { this.replicaPeerInfo = value; }

    @JsonProperty("SizeMB")
    public long getSizeMB() { return sizeMB; }
    @JsonProperty("SizeMB")
    public void setSizeMB(long value) { this.sizeMB = value; }

    @JsonProperty("DisplaySize")
    public String getDisplaySize() { return displaySize; }
    @JsonProperty("DisplaySize")
    public void setDisplaySize(String value) { this.displaySize = value; }

    @JsonProperty("treeViewEntityIcon")
    public String getTreeViewEntityIcon() { return treeViewEntityIcon; }
    @JsonProperty("treeViewEntityIcon")
    public void setTreeViewEntityIcon(String value) { this.treeViewEntityIcon = value; }

    @JsonProperty("DisplayName")
    public String getDisplayName() { return displayName; }
    @JsonProperty("DisplayName")
    public void setDisplayName(String value) { this.displayName = value; }

    @JsonProperty("HighWaterMark")
    public long getHighWaterMark() { return highWaterMark; }
    @JsonProperty("HighWaterMark")
    public void setHighWaterMark(long value) { this.highWaterMark = value; }

    @JsonProperty("Protocol")
    public String getProtocol() { return protocol; }
    @JsonProperty("Protocol")
    public void setProtocol(String value) { this.protocol = value; }

    @JsonProperty("RequiredTierMask")
    public long getRequiredTierMask() { return requiredTierMask; }
    @JsonProperty("RequiredTierMask")
    public void setRequiredTierMask(long value) { this.requiredTierMask = value; }

    @JsonProperty("ScsiVpdNaa")
    public String getSCSIVpdNaa() { return scsiVpdNaa; }
    @JsonProperty("ScsiVpdNaa")
    public void setSCSIVpdNaa(String value) { this.scsiVpdNaa = value; }

    @JsonProperty("EncryptedStatusShort")
    public String getEncryptedStatusShort() { return encryptedStatusShort; }
    @JsonProperty("EncryptedStatusShort")
    public void setEncryptedStatusShort(String value) { this.encryptedStatusShort = value; }

    @JsonProperty("treeViewLogicalDecorator")
    public String getTreeViewLogicalDecorator() { return treeViewLogicalDecorator; }
    @JsonProperty("treeViewLogicalDecorator")
    public void setTreeViewLogicalDecorator(String value) { this.treeViewLogicalDecorator = value; }

    @JsonProperty("Basetype")
    public String getBasetype() { return basetype; }
    @JsonProperty("Basetype")
    public void setBasetype(String value) { this.basetype = value; }

    @JsonProperty("FaultTolerance")
    public long getFaultTolerance() { return faultTolerance; }
    @JsonProperty("FaultTolerance")
    public void setFaultTolerance(long value) { this.faultTolerance = value; }

    @JsonProperty("DisplayGroupId")
    public String getDisplayGroupID() { return displayGroupID; }
    @JsonProperty("DisplayGroupId")
    public void setDisplayGroupID(String value) { this.displayGroupID = value; }

    @JsonProperty("ConnectedInitiatorList")
    public ConnectedInitiatorList getConnectedInitiatorList() { return connectedInitiatorList; }
    @JsonProperty("ConnectedInitiatorList")
    public void setConnectedInitiatorList(ConnectedInitiatorList value) { this.connectedInitiatorList = value; }

    @JsonProperty("DisplayDateTime")
    public String getDisplayDateTime() { return displayDateTime; }
    @JsonProperty("DisplayDateTime")
    public void setDisplayDateTime(String value) { this.displayDateTime = value; }

    @JsonProperty("MetadataSize")
    public long getMetadataSize() { return metadataSize; }
    @JsonProperty("MetadataSize")
    public void setMetadataSize(long value) { this.metadataSize = value; }

    @JsonProperty("SnapshotCount")
    public long getSnapshotCount() { return snapshotCount; }
    @JsonProperty("SnapshotCount")
    public void setSnapshotCount(long value) { this.snapshotCount = value; }

    @JsonProperty("PathTolerance")
    public long getPathTolerance() { return pathTolerance; }
    @JsonProperty("PathTolerance")
    public void setPathTolerance(long value) { this.pathTolerance = value; }

    @JsonProperty("PhysicalSize")
    public long getPhysicalSize() { return physicalSize; }
    @JsonProperty("PhysicalSize")
    public void setPhysicalSize(long value) { this.physicalSize = value; }
}
