package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum NICType {
    BOND, ETHERNET, LINK, VIRTUAL;

    @JsonValue
    public String toValue() {
        switch (this) {
        case BOND: return "Bond";
        case ETHERNET: return "Ethernet";
        case LINK: return "Link";
        case VIRTUAL: return "Virtual";
        }
        return null;
    }

    @JsonCreator
    public static NICType forValue(String value) throws IOException {
        if (value.equals("Bond")) return BOND;
        if (value.equals("Ethernet")) return ETHERNET;
        if (value.equals("Link")) return LINK;
        if (value.equals("Virtual")) return VIRTUAL;
        throw new IOException("Cannot deserialize NICType");
    }
}
