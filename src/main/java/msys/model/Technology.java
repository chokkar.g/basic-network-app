package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum Technology {
    DISK, MLC;

    @JsonValue
    public String toValue() {
        switch (this) {
        case DISK: return "DISK";
        case MLC: return "MLC";
        }
        return null;
    }

    @JsonCreator
    public static Technology forValue(String value) throws IOException {
        if (value.equals("DISK")) return DISK;
        if (value.equals("MLC")) return MLC;
        throw new IOException("Cannot deserialize Technology");
    }
}
