package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum MastershipState {
    UNKNOWN;

    @JsonValue
    public String toValue() {
        switch (this) {
        case UNKNOWN: return "Unknown";
        }
        return null;
    }

    @JsonCreator
    public static MastershipState forValue(String value) throws IOException {
        if (value.equals("Unknown")) return UNKNOWN;
        throw new IOException("Cannot deserialize MastershipState");
    }
}
