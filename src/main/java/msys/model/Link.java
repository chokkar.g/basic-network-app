package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum Link {
    OFFLINE, ONLINE;

    @JsonValue
    public String toValue() {
        switch (this) {
        case OFFLINE: return "Offline";
        case ONLINE: return "Online";
        }
        return null;
    }

    @JsonCreator
    public static Link forValue(String value) throws IOException {
        if (value.equals("Offline")) return OFFLINE;
        if (value.equals("Online")) return ONLINE;
        throw new IOException("Cannot deserialize Link");
    }
}
