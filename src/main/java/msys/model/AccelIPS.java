package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class AccelIPS {
    private List<String> accelIP;

    @JsonProperty("accelIP")
    public List<String> getAccelIP() { return accelIP; }
    @JsonProperty("accelIP")
    public void setAccelIP(List<String> value) { this.accelIP = value; }
}
