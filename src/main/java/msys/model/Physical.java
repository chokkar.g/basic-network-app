package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Physical {
    private String rams;
    private String activeos;
    private String displayActiveOS;
    private Drives drives;
    private String cpus;
    private String standbyos;
    private Environmentals environmentals;
    private String displayStandbyOS;
    private Nics nics;
    private FlashUnion flash;

    @JsonProperty("rams")
    public String getRams() { return rams; }
    @JsonProperty("rams")
    public void setRams(String value) { this.rams = value; }

    @JsonProperty("activeos")
    public String getActiveos() { return activeos; }
    @JsonProperty("activeos")
    public void setActiveos(String value) { this.activeos = value; }

    @JsonProperty("displayActiveOS")
    public String getDisplayActiveOS() { return displayActiveOS; }
    @JsonProperty("displayActiveOS")
    public void setDisplayActiveOS(String value) { this.displayActiveOS = value; }

    @JsonProperty("drives")
    public Drives getDrives() { return drives; }
    @JsonProperty("drives")
    public void setDrives(Drives value) { this.drives = value; }

    @JsonProperty("cpus")
    public String getCpus() { return cpus; }
    @JsonProperty("cpus")
    public void setCpus(String value) { this.cpus = value; }

    @JsonProperty("standbyos")
    public String getStandbyos() { return standbyos; }
    @JsonProperty("standbyos")
    public void setStandbyos(String value) { this.standbyos = value; }

    @JsonProperty("environmentals")
    public Environmentals getEnvironmentals() { return environmentals; }
    @JsonProperty("environmentals")
    public void setEnvironmentals(Environmentals value) { this.environmentals = value; }

    @JsonProperty("displayStandbyOS")
    public String getDisplayStandbyOS() { return displayStandbyOS; }
    @JsonProperty("displayStandbyOS")
    public void setDisplayStandbyOS(String value) { this.displayStandbyOS = value; }

    @JsonProperty("nics")
    public Nics getNics() { return nics; }
    @JsonProperty("nics")
    public void setNics(Nics value) { this.nics = value; }

    @JsonProperty("flash")
    public FlashUnion getFlash() { return flash; }
    @JsonProperty("flash")
    public void setFlash(FlashUnion value) { this.flash = value; }
}
