package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.*;

@JsonDeserialize(using = VolumesUnion.Deserializer.class)
@JsonSerialize(using = VolumesUnion.Serializer.class)
public class VolumesUnion {
    public VolumesVolumes volumesVolumesValue;
    public String stringValue;

    static class Deserializer extends JsonDeserializer<VolumesUnion> {
        @Override
        public VolumesUnion deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            VolumesUnion value = new VolumesUnion();
            switch (jsonParser.getCurrentToken()) {
            case VALUE_STRING:
                value.stringValue = jsonParser.readValueAs(String.class);
                break;
            case START_OBJECT:
                value.volumesVolumesValue = jsonParser.readValueAs(VolumesVolumes.class);
                break;
            default: throw new IOException("Cannot deserialize VolumesUnion");
            }
            return value;
        }
    }

    static class Serializer extends JsonSerializer<VolumesUnion> {
        @Override
        public void serialize(VolumesUnion obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            if (obj.volumesVolumesValue != null) {
                jsonGenerator.writeObject(obj.volumesVolumesValue);
                return;
            }
            if (obj.stringValue != null) {
                jsonGenerator.writeObject(obj.stringValue);
                return;
            }
            throw new IOException("VolumesUnion must not be null");
        }
    }
}
