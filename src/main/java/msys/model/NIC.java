package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class NIC {
    private long mode;
    private Ipaddresses ipaddresses;
    private String name;
    private Link link;
    private LogicalState state;
    private long category;
    private NICType type;
    private String stateIcon;
    private String uuid;
    private long ifnum;
    private SpeedUnion speed;
    private String mac;

    @JsonProperty("mode")
    public long getMode() { return mode; }
    @JsonProperty("mode")
    public void setMode(long value) { this.mode = value; }

    @JsonProperty("ipaddresses")
    public Ipaddresses getIpaddresses() { return ipaddresses; }
    @JsonProperty("ipaddresses")
    public void setIpaddresses(Ipaddresses value) { this.ipaddresses = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("link")
    public Link getLink() { return link; }
    @JsonProperty("link")
    public void setLink(Link value) { this.link = value; }

    @JsonProperty("state")
    public LogicalState getState() { return state; }
    @JsonProperty("state")
    public void setState(LogicalState value) { this.state = value; }

    @JsonProperty("category")
    public long getCategory() { return category; }
    @JsonProperty("category")
    public void setCategory(long value) { this.category = value; }

    @JsonProperty("type")
    public NICType getType() { return type; }
    @JsonProperty("type")
    public void setType(NICType value) { this.type = value; }

    @JsonProperty("stateIcon")
    public String getStateIcon() { return stateIcon; }
    @JsonProperty("stateIcon")
    public void setStateIcon(String value) { this.stateIcon = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("ifnum")
    public long getIfnum() { return ifnum; }
    @JsonProperty("ifnum")
    public void setIfnum(long value) { this.ifnum = value; }

    @JsonProperty("speed")
    public SpeedUnion getSpeed() { return speed; }
    @JsonProperty("speed")
    public void setSpeed(SpeedUnion value) { this.speed = value; }

    @JsonProperty("mac")
    public String getMAC() { return mac; }
    @JsonProperty("mac")
    public void setMAC(String value) { this.mac = value; }
}
