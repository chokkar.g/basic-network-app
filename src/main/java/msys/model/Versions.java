package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Versions {
    private Version version;

    @JsonProperty("version")
    public Version getVersion() { return version; }
    @JsonProperty("version")
    public void setVersion(Version value) { this.version = value; }
}
