package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum Firmware {
    LJ84, LS36;

    @JsonValue
    public String toValue() {
        switch (this) {
        case LJ84: return "LJ84";
        case LS36: return "LS36";
        }
        return null;
    }

    @JsonCreator
    public static Firmware forValue(String value) throws IOException {
        if (value.equals("LJ84")) return LJ84;
        if (value.equals("LS36")) return LS36;
        throw new IOException("Cannot deserialize Firmware");
    }
}
