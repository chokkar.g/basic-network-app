package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Ipaddresses {
    private IpaddressUnion ipaddress;

    @JsonProperty("ipaddress")
    public IpaddressUnion getIpaddress() { return ipaddress; }
    @JsonProperty("ipaddress")
    public void setIpaddress(IpaddressUnion value) { this.ipaddress = value; }
}
