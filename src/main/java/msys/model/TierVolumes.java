package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class TierVolumes {
    private List<PurpleVolume> volume;

    @JsonProperty("volume")
    public List<PurpleVolume> getVolume() { return volume; }
    @JsonProperty("volume")
    public void setVolume(List<PurpleVolume> value) { this.volume = value; }
}
