package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class SystemVolumes {
    private List<SystemVolume> systemVolume;

    @JsonProperty("systemVolume")
    public List<SystemVolume> getSystemVolume() { return systemVolume; }
    @JsonProperty("systemVolume")
    public void setSystemVolume(List<SystemVolume> value) { this.systemVolume = value; }
}
