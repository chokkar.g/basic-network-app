package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Members {
    private List<MembersAppliance> appliance;

    @JsonProperty("appliance")
    public List<MembersAppliance> getAppliance() { return appliance; }
    @JsonProperty("appliance")
    public void setAppliance(List<MembersAppliance> value) { this.appliance = value; }
}
