package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Tier {
    private long tiernum;
    private String treeViewLogicalDecorator;
    private String treeViewPhysicalDecorator;
    private long devicecount;
    private TierVolumes volumes;
    private long type;
    private String uuid;
    private boolean isHidden;
    private Capacity capacity;
    private String arrayuuid;
    private LogicalState logicalState;
    private boolean isCompletelySpecified;
    private String treeViewEntityIcon;
    private Name name;
    private LogicalState state;
    private boolean sparingsupported;

    @JsonProperty("tiernum")
    public long getTiernum() { return tiernum; }
    @JsonProperty("tiernum")
    public void setTiernum(long value) { this.tiernum = value; }

    @JsonProperty("treeViewLogicalDecorator")
    public String getTreeViewLogicalDecorator() { return treeViewLogicalDecorator; }
    @JsonProperty("treeViewLogicalDecorator")
    public void setTreeViewLogicalDecorator(String value) { this.treeViewLogicalDecorator = value; }

    @JsonProperty("treeViewPhysicalDecorator")
    public String getTreeViewPhysicalDecorator() { return treeViewPhysicalDecorator; }
    @JsonProperty("treeViewPhysicalDecorator")
    public void setTreeViewPhysicalDecorator(String value) { this.treeViewPhysicalDecorator = value; }

    @JsonProperty("devicecount")
    public long getDevicecount() { return devicecount; }
    @JsonProperty("devicecount")
    public void setDevicecount(long value) { this.devicecount = value; }

    @JsonProperty("volumes")
    public TierVolumes getVolumes() { return volumes; }
    @JsonProperty("volumes")
    public void setVolumes(TierVolumes value) { this.volumes = value; }

    @JsonProperty("type")
    public long getType() { return type; }
    @JsonProperty("type")
    public void setType(long value) { this.type = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("isHidden")
    public boolean getIsHidden() { return isHidden; }
    @JsonProperty("isHidden")
    public void setIsHidden(boolean value) { this.isHidden = value; }

    @JsonProperty("capacity")
    public Capacity getCapacity() { return capacity; }
    @JsonProperty("capacity")
    public void setCapacity(Capacity value) { this.capacity = value; }

    @JsonProperty("arrayuuid")
    public String getArrayuuid() { return arrayuuid; }
    @JsonProperty("arrayuuid")
    public void setArrayuuid(String value) { this.arrayuuid = value; }

    @JsonProperty("logicalState")
    public LogicalState getLogicalState() { return logicalState; }
    @JsonProperty("logicalState")
    public void setLogicalState(LogicalState value) { this.logicalState = value; }

    @JsonProperty("isCompletelySpecified")
    public boolean getIsCompletelySpecified() { return isCompletelySpecified; }
    @JsonProperty("isCompletelySpecified")
    public void setIsCompletelySpecified(boolean value) { this.isCompletelySpecified = value; }

    @JsonProperty("treeViewEntityIcon")
    public String getTreeViewEntityIcon() { return treeViewEntityIcon; }
    @JsonProperty("treeViewEntityIcon")
    public void setTreeViewEntityIcon(String value) { this.treeViewEntityIcon = value; }

    @JsonProperty("name")
    public Name getName() { return name; }
    @JsonProperty("name")
    public void setName(Name value) { this.name = value; }

    @JsonProperty("state")
    public LogicalState getState() { return state; }
    @JsonProperty("state")
    public void setState(LogicalState value) { this.state = value; }

    @JsonProperty("sparingsupported")
    public boolean getSparingsupported() { return sparingsupported; }
    @JsonProperty("sparingsupported")
    public void setSparingsupported(boolean value) { this.sparingsupported = value; }
}
