package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.*;

@JsonDeserialize(using = PolicyUnion.Deserializer.class)
@JsonSerialize(using = PolicyUnion.Serializer.class)
public class PolicyUnion {
    public PurplePolicy purplePolicyValue;
    public List<PolicyElement> policyElementArrayValue;

    static class Deserializer extends JsonDeserializer<PolicyUnion> {
        @Override
        public PolicyUnion deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            PolicyUnion value = new PolicyUnion();
            switch (jsonParser.getCurrentToken()) {
            case START_ARRAY:
                value.policyElementArrayValue = jsonParser.readValueAs(new TypeReference<List<PolicyElement>>() {});
                break;
            case START_OBJECT:
                value.purplePolicyValue = jsonParser.readValueAs(PurplePolicy.class);
                break;
            default: throw new IOException("Cannot deserialize PolicyUnion");
            }
            return value;
        }
    }

    static class Serializer extends JsonSerializer<PolicyUnion> {
        @Override
        public void serialize(PolicyUnion obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            if (obj.purplePolicyValue != null) {
                jsonGenerator.writeObject(obj.purplePolicyValue);
                return;
            }
            if (obj.policyElementArrayValue != null) {
                jsonGenerator.writeObject(obj.policyElementArrayValue);
                return;
            }
            throw new IOException("PolicyUnion must not be null");
        }
    }
}
