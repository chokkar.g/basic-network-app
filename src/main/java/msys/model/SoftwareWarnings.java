package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class SoftwareWarnings {
    private List<String> warning;

    @JsonProperty("warning")
    public List<String> getWarning() { return warning; }
    @JsonProperty("warning")
    public void setWarning(List<String> value) { this.warning = value; }
}
