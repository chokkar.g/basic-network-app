package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class FlashClass {
    private NvmeInfo nvmeInfo;

    @JsonProperty("nvmeInfo")
    public NvmeInfo getNvmeInfo() { return nvmeInfo; }
    @JsonProperty("nvmeInfo")
    public void setNvmeInfo(NvmeInfo value) { this.nvmeInfo = value; }
}
