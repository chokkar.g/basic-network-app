package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Tiers {
    private Tier tier;

    @JsonProperty("tier")
    public Tier getTier() { return tier; }
    @JsonProperty("tier")
    public void setTier(Tier value) { this.tier = value; }
}
