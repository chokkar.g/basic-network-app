package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum SpeedEnum {
    THE_10_K_IOPS, THE_7200_RPM;

    @JsonValue
    public String toValue() {
        switch (this) {
        case THE_10_K_IOPS: return "10K IOPS";
        case THE_7200_RPM: return "7200 RPM";
        }
        return null;
    }

    @JsonCreator
    public static SpeedEnum forValue(String value) throws IOException {
        if (value.equals("10K IOPS")) return THE_10_K_IOPS;
        if (value.equals("7200 RPM")) return THE_7200_RPM;
        throw new IOException("Cannot deserialize SpeedEnum");
    }
}
