package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Controller {
    private String status;
    private boolean uidLightEnabled;
    private String objectPath;
    private String taskID;
    private String basetype;
    private MastershipState mastershipState;
    private long enclosureSlot;
    private String name;
    private String enclosureSN;
    private String enclosureUUID;
    private String serialNumber;
    private Versions versions;
    private String id;
    private UUID databankUUID;

    @JsonProperty("Status")
    public String getStatus() { return status; }
    @JsonProperty("Status")
    public void setStatus(String value) { this.status = value; }

    @JsonProperty("UIDLightEnabled")
    public boolean getUidLightEnabled() { return uidLightEnabled; }
    @JsonProperty("UIDLightEnabled")
    public void setUidLightEnabled(boolean value) { this.uidLightEnabled = value; }

    @JsonProperty("ObjectPath")
    public String getObjectPath() { return objectPath; }
    @JsonProperty("ObjectPath")
    public void setObjectPath(String value) { this.objectPath = value; }

    @JsonProperty("TaskId")
    public String getTaskID() { return taskID; }
    @JsonProperty("TaskId")
    public void setTaskID(String value) { this.taskID = value; }

    @JsonProperty("Basetype")
    public String getBasetype() { return basetype; }
    @JsonProperty("Basetype")
    public void setBasetype(String value) { this.basetype = value; }

    @JsonProperty("MastershipState")
    public MastershipState getMastershipState() { return mastershipState; }
    @JsonProperty("MastershipState")
    public void setMastershipState(MastershipState value) { this.mastershipState = value; }

    @JsonProperty("EnclosureSlot")
    public long getEnclosureSlot() { return enclosureSlot; }
    @JsonProperty("EnclosureSlot")
    public void setEnclosureSlot(long value) { this.enclosureSlot = value; }

    @JsonProperty("Name")
    public String getName() { return name; }
    @JsonProperty("Name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("EnclosureSN")
    public String getEnclosureSN() { return enclosureSN; }
    @JsonProperty("EnclosureSN")
    public void setEnclosureSN(String value) { this.enclosureSN = value; }

    @JsonProperty("EnclosureUUID")
    public String getEnclosureUUID() { return enclosureUUID; }
    @JsonProperty("EnclosureUUID")
    public void setEnclosureUUID(String value) { this.enclosureUUID = value; }

    @JsonProperty("SerialNumber")
    public String getSerialNumber() { return serialNumber; }
    @JsonProperty("SerialNumber")
    public void setSerialNumber(String value) { this.serialNumber = value; }

    @JsonProperty("versions")
    public Versions getVersions() { return versions; }
    @JsonProperty("versions")
    public void setVersions(Versions value) { this.versions = value; }

    @JsonProperty("id")
    public String getID() { return id; }
    @JsonProperty("id")
    public void setID(String value) { this.id = value; }

    @JsonProperty("DatabankUUID")
    public UUID getDatabankUUID() { return databankUUID; }
    @JsonProperty("DatabankUUID")
    public void setDatabankUUID(UUID value) { this.databankUUID = value; }
}
