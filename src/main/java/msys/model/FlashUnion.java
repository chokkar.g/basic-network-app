package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.*;

@JsonDeserialize(using = FlashUnion.Deserializer.class)
@JsonSerialize(using = FlashUnion.Serializer.class)
public class FlashUnion {
    public FlashClass flashClassValue;
    public String stringValue;

    static class Deserializer extends JsonDeserializer<FlashUnion> {
        @Override
        public FlashUnion deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            FlashUnion value = new FlashUnion();
            switch (jsonParser.getCurrentToken()) {
            case VALUE_STRING:
                value.stringValue = jsonParser.readValueAs(String.class);
                break;
            case START_OBJECT:
                value.flashClassValue = jsonParser.readValueAs(FlashClass.class);
                break;
            default: throw new IOException("Cannot deserialize FlashUnion");
            }
            return value;
        }
    }

    static class Serializer extends JsonSerializer<FlashUnion> {
        @Override
        public void serialize(FlashUnion obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            if (obj.flashClassValue != null) {
                jsonGenerator.writeObject(obj.flashClassValue);
                return;
            }
            if (obj.stringValue != null) {
                jsonGenerator.writeObject(obj.stringValue);
                return;
            }
            throw new IOException("FlashUnion must not be null");
        }
    }
}
