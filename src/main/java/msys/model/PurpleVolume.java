package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class PurpleVolume {
    private String iscsiname;
    private String raidName;
    private String treeViewLogicalDecorator;
    private String treeViewPhysicalDecorator;
    private boolean chapenabled;
    private boolean raidchanging;
    private String initiators;
    private long type;
    private String uuid;
    private boolean isHidden;
    private Capacity capacity;
    private String qos;
    private long tier;
    private LogicalState logicalState;
    private boolean isCompletelySpecified;
    private String treeViewEntityIcon;
    private String name;
    private LogicalState state;
    private boolean multiappliance;
    private long reasonCode;
    private long raid;

    @JsonProperty("iscsiname")
    public String getIscsiname() { return iscsiname; }
    @JsonProperty("iscsiname")
    public void setIscsiname(String value) { this.iscsiname = value; }

    @JsonProperty("raidName")
    public String getRAIDName() { return raidName; }
    @JsonProperty("raidName")
    public void setRAIDName(String value) { this.raidName = value; }

    @JsonProperty("treeViewLogicalDecorator")
    public String getTreeViewLogicalDecorator() { return treeViewLogicalDecorator; }
    @JsonProperty("treeViewLogicalDecorator")
    public void setTreeViewLogicalDecorator(String value) { this.treeViewLogicalDecorator = value; }

    @JsonProperty("treeViewPhysicalDecorator")
    public String getTreeViewPhysicalDecorator() { return treeViewPhysicalDecorator; }
    @JsonProperty("treeViewPhysicalDecorator")
    public void setTreeViewPhysicalDecorator(String value) { this.treeViewPhysicalDecorator = value; }

    @JsonProperty("chapenabled")
    public boolean getChapenabled() { return chapenabled; }
    @JsonProperty("chapenabled")
    public void setChapenabled(boolean value) { this.chapenabled = value; }

    @JsonProperty("raidchanging")
    public boolean getRaidchanging() { return raidchanging; }
    @JsonProperty("raidchanging")
    public void setRaidchanging(boolean value) { this.raidchanging = value; }

    @JsonProperty("initiators")
    public String getInitiators() { return initiators; }
    @JsonProperty("initiators")
    public void setInitiators(String value) { this.initiators = value; }

    @JsonProperty("type")
    public long getType() { return type; }
    @JsonProperty("type")
    public void setType(long value) { this.type = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("isHidden")
    public boolean getIsHidden() { return isHidden; }
    @JsonProperty("isHidden")
    public void setIsHidden(boolean value) { this.isHidden = value; }

    @JsonProperty("capacity")
    public Capacity getCapacity() { return capacity; }
    @JsonProperty("capacity")
    public void setCapacity(Capacity value) { this.capacity = value; }

    @JsonProperty("qos")
    public String getQos() { return qos; }
    @JsonProperty("qos")
    public void setQos(String value) { this.qos = value; }

    @JsonProperty("tier")
    public long getTier() { return tier; }
    @JsonProperty("tier")
    public void setTier(long value) { this.tier = value; }

    @JsonProperty("logicalState")
    public LogicalState getLogicalState() { return logicalState; }
    @JsonProperty("logicalState")
    public void setLogicalState(LogicalState value) { this.logicalState = value; }

    @JsonProperty("isCompletelySpecified")
    public boolean getIsCompletelySpecified() { return isCompletelySpecified; }
    @JsonProperty("isCompletelySpecified")
    public void setIsCompletelySpecified(boolean value) { this.isCompletelySpecified = value; }

    @JsonProperty("treeViewEntityIcon")
    public String getTreeViewEntityIcon() { return treeViewEntityIcon; }
    @JsonProperty("treeViewEntityIcon")
    public void setTreeViewEntityIcon(String value) { this.treeViewEntityIcon = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("state")
    public LogicalState getState() { return state; }
    @JsonProperty("state")
    public void setState(LogicalState value) { this.state = value; }

    @JsonProperty("multiappliance")
    public boolean getMultiappliance() { return multiappliance; }
    @JsonProperty("multiappliance")
    public void setMultiappliance(boolean value) { this.multiappliance = value; }

    @JsonProperty("reasonCode")
    public long getReasonCode() { return reasonCode; }
    @JsonProperty("reasonCode")
    public void setReasonCode(long value) { this.reasonCode = value; }

    @JsonProperty("raid")
    public long getRAID() { return raid; }
    @JsonProperty("raid")
    public void setRAID(long value) { this.raid = value; }
}
