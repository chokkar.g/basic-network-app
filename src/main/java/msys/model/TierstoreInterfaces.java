package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class TierstoreInterfaces {
    private List<TierstoreInterfacesAppliance> appliance;

    @JsonProperty("appliance")
    public List<TierstoreInterfacesAppliance> getAppliance() { return appliance; }
    @JsonProperty("appliance")
    public void setAppliance(List<TierstoreInterfacesAppliance> value) { this.appliance = value; }
}
