package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class ConnectedInitiatorList {
    private List<String> initiators;

    @JsonProperty("Initiators")
    public List<String> getInitiators() { return initiators; }
    @JsonProperty("Initiators")
    public void setInitiators(List<String> value) { this.initiators = value; }
}
