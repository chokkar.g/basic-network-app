package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum DriveType {
    SATA_DISK, SATA_MLC;

    @JsonValue
    public String toValue() {
        switch (this) {
        case SATA_DISK: return "SATA:DISK";
        case SATA_MLC: return "SATA:MLC";
        }
        return null;
    }

    @JsonCreator
    public static DriveType forValue(String value) throws IOException {
        if (value.equals("SATA:DISK")) return SATA_DISK;
        if (value.equals("SATA:MLC")) return SATA_MLC;
        throw new IOException("Cannot deserialize DriveType");
    }
}
