package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Environmentals {
    private String powersupplies;
    private String tempsensors;
    private String fans;

    @JsonProperty("powersupplies")
    public String getPowersupplies() { return powersupplies; }
    @JsonProperty("powersupplies")
    public void setPowersupplies(String value) { this.powersupplies = value; }

    @JsonProperty("tempsensors")
    public String getTempsensors() { return tempsensors; }
    @JsonProperty("tempsensors")
    public void setTempsensors(String value) { this.tempsensors = value; }

    @JsonProperty("fans")
    public String getFans() { return fans; }
    @JsonProperty("fans")
    public void setFans(String value) { this.fans = value; }
}
