package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Policies {
    private PolicyUnion policy;

    @JsonProperty("policy")
    public PolicyUnion getPolicy() { return policy; }
    @JsonProperty("policy")
    public void setPolicy(PolicyUnion value) { this.policy = value; }
}
