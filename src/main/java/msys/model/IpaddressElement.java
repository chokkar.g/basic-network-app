package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class IpaddressElement {
    private String ipaddress;
    private long ipSECPort;
    private UUID associateduuid;
    private long iscsiPort;
    private IpaddressType type;
    private Subnetmask subnetmask;
    private Gateway gateway;

    @JsonProperty("ipaddress")
    public String getIpaddress() { return ipaddress; }
    @JsonProperty("ipaddress")
    public void setIpaddress(String value) { this.ipaddress = value; }

    @JsonProperty("ipSecPort")
    public long getIPSECPort() { return ipSECPort; }
    @JsonProperty("ipSecPort")
    public void setIPSECPort(long value) { this.ipSECPort = value; }

    @JsonProperty("associateduuid")
    public UUID getAssociateduuid() { return associateduuid; }
    @JsonProperty("associateduuid")
    public void setAssociateduuid(UUID value) { this.associateduuid = value; }

    @JsonProperty("iscsiPort")
    public long getIscsiPort() { return iscsiPort; }
    @JsonProperty("iscsiPort")
    public void setIscsiPort(long value) { this.iscsiPort = value; }

    @JsonProperty("type")
    public IpaddressType getType() { return type; }
    @JsonProperty("type")
    public void setType(IpaddressType value) { this.type = value; }

    @JsonProperty("subnetmask")
    public Subnetmask getSubnetmask() { return subnetmask; }
    @JsonProperty("subnetmask")
    public void setSubnetmask(Subnetmask value) { this.subnetmask = value; }

    @JsonProperty("gateway")
    public Gateway getGateway() { return gateway; }
    @JsonProperty("gateway")
    public void setGateway(Gateway value) { this.gateway = value; }
}
