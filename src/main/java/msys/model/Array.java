package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class Array {
    private boolean failoversupported;
    private Pools pools;
    private long type;
    private String uuid;
    private String hardwareWarnings;
    private String failoverstatus;
    private boolean isMaintenanceModeSupported;
    private String displayTotalUsed;
    private LogicalState logicalState;
    private boolean isTierStoreCompletelySpecified;
    private Members members;
    private Controllers controllers;
    private LogicalState state;
    private boolean isMaintenanceModeAvailable;
    private String upgradestate;
    private String rebuildpriority;
    private boolean isAccelerationArray;
    private boolean ufccapable;
    private boolean hasEvalLicense;
    private boolean alarmsounding;
    private long totalUsedMB;
    private boolean autohealAvailable;
    private long totalProvisionedMB;
    private String name;
    private TierstoreInterfaces tierstoreInterfaces;
    private boolean isApplianceSparingSupported;
    private boolean snmpenabled;
    private Tiers tiers;
    private SystemPools systemPools;
    private AccelIPS accelIPS;
    private String displayTotalAvailable;
    private String totalSnapUsed;
    private String displayTotalProvisioned;
    private long totalAvailableMB;
    private boolean isCompletelySpecified;
    private String treeViewEntityIcon;
    private boolean autohealEnabled;
    private boolean alarmenabled;
    private boolean mchapdefined;
    private long reasonCode;
    private long keyServerClusterLimit;
    private String treeViewLogicalDecorator;
    private String treeViewPhysicalDecorator;
    private String autohealAvailability;
    private long totalSnapUsedMB;
    private boolean isaccelerated;
    private String maintenanceModeAvailibilityDescription;
    private boolean isHidden;
    private boolean hasNodesWithNoIPAddresses;
    private long chapModeVersion;
    private SystemIdentities systemIdentities;
    private boolean waitingForRemediationApproval;
    private boolean globalMutualChapEnabled;
    private String upgradestateicon;
    private boolean failoverreliable;
    private UserVolumes userVolumes;
    private boolean tierstoreConfigurationAllowed;
    private SoftwareWarnings softwareWarnings;

    @JsonProperty("failoversupported")
    public boolean getFailoversupported() { return failoversupported; }
    @JsonProperty("failoversupported")
    public void setFailoversupported(boolean value) { this.failoversupported = value; }

    @JsonProperty("pools")
    public Pools getPools() { return pools; }
    @JsonProperty("pools")
    public void setPools(Pools value) { this.pools = value; }

    @JsonProperty("type")
    public long getType() { return type; }
    @JsonProperty("type")
    public void setType(long value) { this.type = value; }

    @JsonProperty("uuid")
    public String getUUID() { return uuid; }
    @JsonProperty("uuid")
    public void setUUID(String value) { this.uuid = value; }

    @JsonProperty("hardwareWarnings")
    public String getHardwareWarnings() { return hardwareWarnings; }
    @JsonProperty("hardwareWarnings")
    public void setHardwareWarnings(String value) { this.hardwareWarnings = value; }

    @JsonProperty("failoverstatus")
    public String getFailoverstatus() { return failoverstatus; }
    @JsonProperty("failoverstatus")
    public void setFailoverstatus(String value) { this.failoverstatus = value; }

    @JsonProperty("isMaintenanceModeSupported")
    public boolean getIsMaintenanceModeSupported() { return isMaintenanceModeSupported; }
    @JsonProperty("isMaintenanceModeSupported")
    public void setIsMaintenanceModeSupported(boolean value) { this.isMaintenanceModeSupported = value; }

    @JsonProperty("DisplayTotalUsed")
    public String getDisplayTotalUsed() { return displayTotalUsed; }
    @JsonProperty("DisplayTotalUsed")
    public void setDisplayTotalUsed(String value) { this.displayTotalUsed = value; }

    @JsonProperty("logicalState")
    public LogicalState getLogicalState() { return logicalState; }
    @JsonProperty("logicalState")
    public void setLogicalState(LogicalState value) { this.logicalState = value; }

    @JsonProperty("isTierStoreCompletelySpecified")
    public boolean getIsTierStoreCompletelySpecified() { return isTierStoreCompletelySpecified; }
    @JsonProperty("isTierStoreCompletelySpecified")
    public void setIsTierStoreCompletelySpecified(boolean value) { this.isTierStoreCompletelySpecified = value; }

    @JsonProperty("members")
    public Members getMembers() { return members; }
    @JsonProperty("members")
    public void setMembers(Members value) { this.members = value; }

    @JsonProperty("controllers")
    public Controllers getControllers() { return controllers; }
    @JsonProperty("controllers")
    public void setControllers(Controllers value) { this.controllers = value; }

    @JsonProperty("state")
    public LogicalState getState() { return state; }
    @JsonProperty("state")
    public void setState(LogicalState value) { this.state = value; }

    @JsonProperty("isMaintenanceModeAvailable")
    public boolean getIsMaintenanceModeAvailable() { return isMaintenanceModeAvailable; }
    @JsonProperty("isMaintenanceModeAvailable")
    public void setIsMaintenanceModeAvailable(boolean value) { this.isMaintenanceModeAvailable = value; }

    @JsonProperty("upgradestate")
    public String getUpgradestate() { return upgradestate; }
    @JsonProperty("upgradestate")
    public void setUpgradestate(String value) { this.upgradestate = value; }

    @JsonProperty("rebuildpriority")
    public String getRebuildpriority() { return rebuildpriority; }
    @JsonProperty("rebuildpriority")
    public void setRebuildpriority(String value) { this.rebuildpriority = value; }

    @JsonProperty("isAccelerationArray")
    public boolean getIsAccelerationArray() { return isAccelerationArray; }
    @JsonProperty("isAccelerationArray")
    public void setIsAccelerationArray(boolean value) { this.isAccelerationArray = value; }

    @JsonProperty("ufccapable")
    public boolean getUfccapable() { return ufccapable; }
    @JsonProperty("ufccapable")
    public void setUfccapable(boolean value) { this.ufccapable = value; }

    @JsonProperty("hasEvalLicense")
    public boolean getHasEvalLicense() { return hasEvalLicense; }
    @JsonProperty("hasEvalLicense")
    public void setHasEvalLicense(boolean value) { this.hasEvalLicense = value; }

    @JsonProperty("alarmsounding")
    public boolean getAlarmsounding() { return alarmsounding; }
    @JsonProperty("alarmsounding")
    public void setAlarmsounding(boolean value) { this.alarmsounding = value; }

    @JsonProperty("TotalUsedMB")
    public long getTotalUsedMB() { return totalUsedMB; }
    @JsonProperty("TotalUsedMB")
    public void setTotalUsedMB(long value) { this.totalUsedMB = value; }

    @JsonProperty("autohealAvailable")
    public boolean getAutohealAvailable() { return autohealAvailable; }
    @JsonProperty("autohealAvailable")
    public void setAutohealAvailable(boolean value) { this.autohealAvailable = value; }

    @JsonProperty("TotalProvisionedMB")
    public long getTotalProvisionedMB() { return totalProvisionedMB; }
    @JsonProperty("TotalProvisionedMB")
    public void setTotalProvisionedMB(long value) { this.totalProvisionedMB = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("tierstoreInterfaces")
    public TierstoreInterfaces getTierstoreInterfaces() { return tierstoreInterfaces; }
    @JsonProperty("tierstoreInterfaces")
    public void setTierstoreInterfaces(TierstoreInterfaces value) { this.tierstoreInterfaces = value; }

    @JsonProperty("isApplianceSparingSupported")
    public boolean getIsApplianceSparingSupported() { return isApplianceSparingSupported; }
    @JsonProperty("isApplianceSparingSupported")
    public void setIsApplianceSparingSupported(boolean value) { this.isApplianceSparingSupported = value; }

    @JsonProperty("snmpenabled")
    public boolean getSnmpenabled() { return snmpenabled; }
    @JsonProperty("snmpenabled")
    public void setSnmpenabled(boolean value) { this.snmpenabled = value; }

    @JsonProperty("tiers")
    public Tiers getTiers() { return tiers; }
    @JsonProperty("tiers")
    public void setTiers(Tiers value) { this.tiers = value; }

    @JsonProperty("systemPools")
    public SystemPools getSystemPools() { return systemPools; }
    @JsonProperty("systemPools")
    public void setSystemPools(SystemPools value) { this.systemPools = value; }

    @JsonProperty("accelIPs")
    public AccelIPS getAccelIPS() { return accelIPS; }
    @JsonProperty("accelIPs")
    public void setAccelIPS(AccelIPS value) { this.accelIPS = value; }

    @JsonProperty("DisplayTotalAvailable")
    public String getDisplayTotalAvailable() { return displayTotalAvailable; }
    @JsonProperty("DisplayTotalAvailable")
    public void setDisplayTotalAvailable(String value) { this.displayTotalAvailable = value; }

    @JsonProperty("TotalSnapUsed")
    public String getTotalSnapUsed() { return totalSnapUsed; }
    @JsonProperty("TotalSnapUsed")
    public void setTotalSnapUsed(String value) { this.totalSnapUsed = value; }

    @JsonProperty("DisplayTotalProvisioned")
    public String getDisplayTotalProvisioned() { return displayTotalProvisioned; }
    @JsonProperty("DisplayTotalProvisioned")
    public void setDisplayTotalProvisioned(String value) { this.displayTotalProvisioned = value; }

    @JsonProperty("TotalAvailableMB")
    public long getTotalAvailableMB() { return totalAvailableMB; }
    @JsonProperty("TotalAvailableMB")
    public void setTotalAvailableMB(long value) { this.totalAvailableMB = value; }

    @JsonProperty("isCompletelySpecified")
    public boolean getIsCompletelySpecified() { return isCompletelySpecified; }
    @JsonProperty("isCompletelySpecified")
    public void setIsCompletelySpecified(boolean value) { this.isCompletelySpecified = value; }

    @JsonProperty("treeViewEntityIcon")
    public String getTreeViewEntityIcon() { return treeViewEntityIcon; }
    @JsonProperty("treeViewEntityIcon")
    public void setTreeViewEntityIcon(String value) { this.treeViewEntityIcon = value; }

    @JsonProperty("autohealEnabled")
    public boolean getAutohealEnabled() { return autohealEnabled; }
    @JsonProperty("autohealEnabled")
    public void setAutohealEnabled(boolean value) { this.autohealEnabled = value; }

    @JsonProperty("alarmenabled")
    public boolean getAlarmenabled() { return alarmenabled; }
    @JsonProperty("alarmenabled")
    public void setAlarmenabled(boolean value) { this.alarmenabled = value; }

    @JsonProperty("mchapdefined")
    public boolean getMchapdefined() { return mchapdefined; }
    @JsonProperty("mchapdefined")
    public void setMchapdefined(boolean value) { this.mchapdefined = value; }

    @JsonProperty("reasonCode")
    public long getReasonCode() { return reasonCode; }
    @JsonProperty("reasonCode")
    public void setReasonCode(long value) { this.reasonCode = value; }

    @JsonProperty("KeyServerClusterLimit")
    public long getKeyServerClusterLimit() { return keyServerClusterLimit; }
    @JsonProperty("KeyServerClusterLimit")
    public void setKeyServerClusterLimit(long value) { this.keyServerClusterLimit = value; }

    @JsonProperty("treeViewLogicalDecorator")
    public String getTreeViewLogicalDecorator() { return treeViewLogicalDecorator; }
    @JsonProperty("treeViewLogicalDecorator")
    public void setTreeViewLogicalDecorator(String value) { this.treeViewLogicalDecorator = value; }

    @JsonProperty("treeViewPhysicalDecorator")
    public String getTreeViewPhysicalDecorator() { return treeViewPhysicalDecorator; }
    @JsonProperty("treeViewPhysicalDecorator")
    public void setTreeViewPhysicalDecorator(String value) { this.treeViewPhysicalDecorator = value; }

    @JsonProperty("autohealAvailability")
    public String getAutohealAvailability() { return autohealAvailability; }
    @JsonProperty("autohealAvailability")
    public void setAutohealAvailability(String value) { this.autohealAvailability = value; }

    @JsonProperty("TotalSnapUsedMB")
    public long getTotalSnapUsedMB() { return totalSnapUsedMB; }
    @JsonProperty("TotalSnapUsedMB")
    public void setTotalSnapUsedMB(long value) { this.totalSnapUsedMB = value; }

    @JsonProperty("isaccelerated")
    public boolean getIsaccelerated() { return isaccelerated; }
    @JsonProperty("isaccelerated")
    public void setIsaccelerated(boolean value) { this.isaccelerated = value; }

    @JsonProperty("maintenanceModeAvailibilityDescription")
    public String getMaintenanceModeAvailibilityDescription() { return maintenanceModeAvailibilityDescription; }
    @JsonProperty("maintenanceModeAvailibilityDescription")
    public void setMaintenanceModeAvailibilityDescription(String value) { this.maintenanceModeAvailibilityDescription = value; }

    @JsonProperty("isHidden")
    public boolean getIsHidden() { return isHidden; }
    @JsonProperty("isHidden")
    public void setIsHidden(boolean value) { this.isHidden = value; }

    @JsonProperty("hasNodesWithNoIPAddresses")
    public boolean getHasNodesWithNoIPAddresses() { return hasNodesWithNoIPAddresses; }
    @JsonProperty("hasNodesWithNoIPAddresses")
    public void setHasNodesWithNoIPAddresses(boolean value) { this.hasNodesWithNoIPAddresses = value; }

    @JsonProperty("chapModeVersion")
    public long getChapModeVersion() { return chapModeVersion; }
    @JsonProperty("chapModeVersion")
    public void setChapModeVersion(long value) { this.chapModeVersion = value; }

    @JsonProperty("systemIdentities")
    public SystemIdentities getSystemIdentities() { return systemIdentities; }
    @JsonProperty("systemIdentities")
    public void setSystemIdentities(SystemIdentities value) { this.systemIdentities = value; }

    @JsonProperty("waitingForRemediationApproval")
    public boolean getWaitingForRemediationApproval() { return waitingForRemediationApproval; }
    @JsonProperty("waitingForRemediationApproval")
    public void setWaitingForRemediationApproval(boolean value) { this.waitingForRemediationApproval = value; }

    @JsonProperty("globalMutualChapEnabled")
    public boolean getGlobalMutualChapEnabled() { return globalMutualChapEnabled; }
    @JsonProperty("globalMutualChapEnabled")
    public void setGlobalMutualChapEnabled(boolean value) { this.globalMutualChapEnabled = value; }

    @JsonProperty("upgradestateicon")
    public String getUpgradestateicon() { return upgradestateicon; }
    @JsonProperty("upgradestateicon")
    public void setUpgradestateicon(String value) { this.upgradestateicon = value; }

    @JsonProperty("failoverreliable")
    public boolean getFailoverreliable() { return failoverreliable; }
    @JsonProperty("failoverreliable")
    public void setFailoverreliable(boolean value) { this.failoverreliable = value; }

    @JsonProperty("userVolumes")
    public UserVolumes getUserVolumes() { return userVolumes; }
    @JsonProperty("userVolumes")
    public void setUserVolumes(UserVolumes value) { this.userVolumes = value; }

    @JsonProperty("tierstoreConfigurationAllowed")
    public boolean getTierstoreConfigurationAllowed() { return tierstoreConfigurationAllowed; }
    @JsonProperty("tierstoreConfigurationAllowed")
    public void setTierstoreConfigurationAllowed(boolean value) { this.tierstoreConfigurationAllowed = value; }

    @JsonProperty("softwareWarnings")
    public SoftwareWarnings getSoftwareWarnings() { return softwareWarnings; }
    @JsonProperty("softwareWarnings")
    public void setSoftwareWarnings(SoftwareWarnings value) { this.softwareWarnings = value; }
	@Override
	public String toString() {
		return "Array [failoversupported=" + failoversupported + ", pools=" + pools + ", type=" + type + ", uuid="
				+ uuid + ", hardwareWarnings=" + hardwareWarnings + ", failoverstatus=" + failoverstatus
				+ ", isMaintenanceModeSupported=" + isMaintenanceModeSupported + ", displayTotalUsed="
				+ displayTotalUsed + ", logicalState=" + logicalState + ", isTierStoreCompletelySpecified="
				+ isTierStoreCompletelySpecified + ", members=" + members + ", controllers=" + controllers + ", state="
				+ state + ", isMaintenanceModeAvailable=" + isMaintenanceModeAvailable + ", upgradestate="
				+ upgradestate + ", rebuildpriority=" + rebuildpriority + ", isAccelerationArray=" + isAccelerationArray
				+ ", ufccapable=" + ufccapable + ", hasEvalLicense=" + hasEvalLicense + ", alarmsounding="
				+ alarmsounding + ", totalUsedMB=" + totalUsedMB + ", autohealAvailable=" + autohealAvailable
				+ ", totalProvisionedMB=" + totalProvisionedMB + ", name=" + name + ", tierstoreInterfaces="
				+ tierstoreInterfaces + ", isApplianceSparingSupported=" + isApplianceSparingSupported
				+ ", snmpenabled=" + snmpenabled + ", tiers=" + tiers + ", systemPools=" + systemPools + ", accelIPS="
				+ accelIPS + ", displayTotalAvailable=" + displayTotalAvailable + ", totalSnapUsed=" + totalSnapUsed
				+ ", displayTotalProvisioned=" + displayTotalProvisioned + ", totalAvailableMB=" + totalAvailableMB
				+ ", isCompletelySpecified=" + isCompletelySpecified + ", treeViewEntityIcon=" + treeViewEntityIcon
				+ ", autohealEnabled=" + autohealEnabled + ", alarmenabled=" + alarmenabled + ", mchapdefined="
				+ mchapdefined + ", reasonCode=" + reasonCode + ", keyServerClusterLimit=" + keyServerClusterLimit
				+ ", treeViewLogicalDecorator=" + treeViewLogicalDecorator + ", treeViewPhysicalDecorator="
				+ treeViewPhysicalDecorator + ", autohealAvailability=" + autohealAvailability + ", totalSnapUsedMB="
				+ totalSnapUsedMB + ", isaccelerated=" + isaccelerated + ", maintenanceModeAvailibilityDescription="
				+ maintenanceModeAvailibilityDescription + ", isHidden=" + isHidden + ", hasNodesWithNoIPAddresses="
				+ hasNodesWithNoIPAddresses + ", chapModeVersion=" + chapModeVersion + ", systemIdentities="
				+ systemIdentities + ", waitingForRemediationApproval=" + waitingForRemediationApproval
				+ ", globalMutualChapEnabled=" + globalMutualChapEnabled + ", upgradestateicon=" + upgradestateicon
				+ ", failoverreliable=" + failoverreliable + ", userVolumes=" + userVolumes
				+ ", tierstoreConfigurationAllowed=" + tierstoreConfigurationAllowed + ", softwareWarnings="
				+ softwareWarnings + "]";
	}
    
    
}
