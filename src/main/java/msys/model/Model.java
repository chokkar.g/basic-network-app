package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.annotation.*;

public enum Model {
    SSDSC2_BX480_G4_N, ST1000_NM0055;

    @JsonValue
    public String toValue() {
        switch (this) {
        case SSDSC2_BX480_G4_N: return "SSDSC2BX480G4N";
        case ST1000_NM0055: return "ST1000NM0055";
        }
        return null;
    }

    @JsonCreator
    public static Model forValue(String value) throws IOException {
        if (value.equals("SSDSC2BX480G4N")) return SSDSC2_BX480_G4_N;
        if (value.equals("ST1000NM0055")) return ST1000_NM0055;
        throw new IOException("Cannot deserialize Model");
    }
}
