package msys.model;

import java.util.*;
import com.fasterxml.jackson.annotation.*;

public class PolicyVolumesClass {
    private List<TentacledVolume> volume;

    @JsonProperty("volume")
    public List<TentacledVolume> getVolume() { return volume; }
    @JsonProperty("volume")
    public void setVolume(List<TentacledVolume> value) { this.volume = value; }
}
