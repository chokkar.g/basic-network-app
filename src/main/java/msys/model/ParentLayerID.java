package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.*;

@JsonDeserialize(using = ParentLayerID.Deserializer.class)
@JsonSerialize(using = ParentLayerID.Serializer.class)
public class ParentLayerID {
    public Long integerValue;
    public String stringValue;

    static class Deserializer extends JsonDeserializer<ParentLayerID> {
        @Override
        public ParentLayerID deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            ParentLayerID value = new ParentLayerID();
            switch (jsonParser.getCurrentToken()) {
            case VALUE_NUMBER_INT:
                value.integerValue = jsonParser.readValueAs(Long.class);
                break;
            case VALUE_STRING:
                value.stringValue = jsonParser.readValueAs(String.class);
                break;
            default: throw new IOException("Cannot deserialize ParentLayerID");
            }
            return value;
        }
    }

    static class Serializer extends JsonSerializer<ParentLayerID> {
        @Override
        public void serialize(ParentLayerID obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            if (obj.integerValue != null) {
                jsonGenerator.writeObject(obj.integerValue);
                return;
            }
            if (obj.stringValue != null) {
                jsonGenerator.writeObject(obj.stringValue);
                return;
            }
            throw new IOException("ParentLayerID must not be null");
        }
    }
}
