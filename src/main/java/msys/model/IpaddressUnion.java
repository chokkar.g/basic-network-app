package msys.model;

import java.util.*;
import java.io.IOException;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.*;

@JsonDeserialize(using = IpaddressUnion.Deserializer.class)
@JsonSerialize(using = IpaddressUnion.Serializer.class)
public class IpaddressUnion {
    public List<IpaddressElement> ipaddressElementArrayValue;
    public IpaddressElement ipaddressElementValue;

    static class Deserializer extends JsonDeserializer<IpaddressUnion> {
        @Override
        public IpaddressUnion deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            IpaddressUnion value = new IpaddressUnion();
            switch (jsonParser.getCurrentToken()) {
            case START_ARRAY:
                value.ipaddressElementArrayValue = jsonParser.readValueAs(new TypeReference<List<IpaddressElement>>() {});
                break;
            case START_OBJECT:
                value.ipaddressElementValue = jsonParser.readValueAs(IpaddressElement.class);
                break;
            default: throw new IOException("Cannot deserialize IpaddressUnion");
            }
            return value;
        }
    }

    static class Serializer extends JsonSerializer<IpaddressUnion> {
        @Override
        public void serialize(IpaddressUnion obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            if (obj.ipaddressElementArrayValue != null) {
                jsonGenerator.writeObject(obj.ipaddressElementArrayValue);
                return;
            }
            if (obj.ipaddressElementValue != null) {
                jsonGenerator.writeObject(obj.ipaddressElementValue);
                return;
            }
            throw new IOException("IpaddressUnion must not be null");
        }
    }
}
