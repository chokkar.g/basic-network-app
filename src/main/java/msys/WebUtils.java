package msys;

import java.io.IOException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import msys.model.Array;
import msys.model.Arrays;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.Route;


public class WebUtils {
	
	private static Gson gson;
	private static ObjectMapper objectMapper;
	
	public WebUtils() {
		
	}

	public static Response fetch(String url, String username, String password) throws Exception {

		OkHttpClient httpClient = createAuthenticatedClient(username, password);
		// execute request
		return doRequest(httpClient, url);

	}

	private static OkHttpClient createAuthenticatedClient(final String username,
	        final String password) throws KeyManagementException {
		// build client with authentication information.
		
        // Create a trust manager that does not validate certificate chains
        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                                   String authType) throws CertificateException {
                    }

                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                                   String authType) throws CertificateException {
                    }

                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                }
        };

		
		 SSLContext sslContext = null;
		try {
			sslContext = SSLContext.getInstance("SSL");
			 sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        

        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

		OkHttpClient httpClient = new OkHttpClient.Builder()
				.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
				.hostnameVerifier(new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                }
)
				.authenticator(new Authenticator() {
			public Request authenticate(Route route, Response response) throws IOException {
				String credential = Credentials.basic(username, password);
				if (responseCount(response) >= 3) {
					return null;
				}
				return response.request().newBuilder().header("Authorization", credential).build();
			}
		}).build();
		
		return httpClient;
	}

	private static Response doRequest(OkHttpClient httpClient, String anyURL) throws Exception {
		objectMapper = new ObjectMapper();
		gson = new Gson();
		Request request = new Request.Builder().url(anyURL).build();
		Response response = httpClient.newCall(request).execute();
		
//		String responseData = response.body().string();
		ResponseBody responseBodyCopy = response.peekBody(Long.MAX_VALUE);
		String responseData = responseBodyCopy.string();
		
//		ResponseBody responseBody = response.body();
//		String responseData = responseBody.string();
		
		System.out.println("Response data: "+responseData);
		
		Arrays arrays = objectMapper.readValue(responseData, Arrays.class);
//		Array array = gson.fromJson(responseData, Array.class);
		System.out.println("Whole Array data:" +arrays);

		if (!response.isSuccessful()) {
			throw new IOException("Unexpected code " + response);
		}
		
		return response;
	}

	private static int responseCount(Response response) {
		int result = 1;
		while ((response = response.priorResponse()) != null) {
			result++;
		}
		return result;
	}
}
